import os

import mysql.connector as cnx


DB_HOST = os.getenv('DB_HOST')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
DB_NAME = os.getenv('DB_NAME')


class dbConn():
    def __init__(self):
        self.needs_commit = False
        self.db = cnx.connect(
            host=DB_HOST,
            user=DB_USER,
            passwd=DB_PASS,
            db=DB_NAME
        )

    def q_ro(self, query, args=(), one=False):
        cur = self.db.cursor()
        cur.execute(query, args)
        result = cur.fetchall()
        cur.close()
        return (result[0] if result else []) if one else result

    def q_wo(self, query, args=(), commit_now=False):
        cur = self.db.cursor()
        cur.execute(query, args)
        cur.close()

        if commit_now:
            self.db.commit()
        else:
            self.needs_commit = True

    def __del__(self):
        if self.needs_commit:
            self.db.commit()
        if hasattr(self.db, 'close'):
            self.db.close()


def get_db_conn(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.get_transaction
    Loads a DB connection ready for use by subsequent hooks. This hook should be loaded as a dependency as it will check if a db connection already exists and ensure it is still healthy.
    '''
    data_pool['db'] = dbConn()
    return data_pool


def get_transaction(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.get_transaction
    Fetches all of the information of the transaction and transaction's type
    '''
    txid = data_pool.get('txid')
    db = data_pool.get('db')
    tx_data = db.q_ro(
        '''SELECT tx.*, txt.* FROM transaction tx
        JOIN tx_type txt ON tx.type=txt.type_id
        WHERE tx.txid=%s;''',
        (txid,),
        one=True
    )

    assert bool(tx_data), 'Invalid TXID provided'

    tx_data = {
        'txid' : tx_data[0],
        'time' : tx_data[1],
        'type' : tx_data[2],
        'sender' : tx_data[3],
        'receiver' : tx_data[4],
        'amount_sent' : round(tx_data[5], 8),
        'amount_recv' : round(tx_data[6], 8),
        'fee' : tx_data[7],
        'confirmations' : tx_data[8],
        'meta' : tx_data[9],
        'cleared' : tx_data[10],
        'type_id' : tx_data[11],
        'type_name' : tx_data[12],
        'description' : tx_data[13],
        'fee' : tx_data[14],
        'free_sender' : tx_data[15],
        'no_receiver' : tx_data[16]
    }

    data_pool.update(tx_data)

    return data_pool


def tx_is_cleared(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.tx_is_cleared
    Fails if the provided transaction is already cleared. This is to avoid doubling-up on the process of a transaction.
    '''
    assert not bool(data_pool.get('cleared', True))
    return data_pool


def tx_is_confirmed(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.tx_is_confirmed
    Fails if the provided transaction does not have the minimum number of confirmations. This is so that deposits can be managed easily.
    '''
    assert data_pool.get('confirmations', 0) >= int(os.getenv('DEPOSIT_CONFIRMATIONS', 1))
    return data_pool


def get_users(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.get_users
    Fetches all of the information of the sender and receiver
    '''
    sender = data_pool.get('sender')
    receiver = data_pool.get('receiver')
    db = data_pool.get('db')

    user_sender = db.q_ro('SELECT * FROM user WHERE username=%s;', (sender,), one=True)
    # This will never happen but it's here anyway
    assert bool(user_sender), 'The transaction sender is invalid'

    user_receiver = db.q_ro('SELECT * FROM user WHERE username=%s;', (receiver,), one=True)
    # This will never happen but it's here anyway
    assert bool(user_sender), 'The transaction receiver is invalid'

    data_pool['user_sender'] = {
      'username' : user_sender[0],
      'address' : user_sender[1],
      'balance' : round(user_sender[2], 8),
      'donations' : user_sender[3],
      'time_created' : user_sender[4],
      'is_banned' : user_sender[5],
      'last_faucet' : user_sender[6]
    }

    data_pool['user_receiver'] = {
      'username' : user_receiver[0],
      'address' : user_receiver[1],
      'balance' : round(user_receiver[2], 8),
      'donations' : user_receiver[3],
      'time_created' : user_receiver[4],
      'is_banned' : user_receiver[5],
      'last_faucet' : user_receiver[6]
    }

    data_pool['sender_old_bal'] = round(user_sender[2], 8)
    data_pool['receiver_old_bal'] = round(user_receiver[2], 8)

    return data_pool


def calc_result_of_tx(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.calc_result_of_tx
    Calculates the final balances of the transaction
    '''
    amount_sent = data_pool.get('amount_sent')
    amount_recv = data_pool.get('amount_recv')
    sender_old_bal = data_pool.get('sender_old_bal')
    receiver_old_bal = data_pool.get('receiver_old_bal')
    free_sender = data_pool.get('free_sender')
    no_receiver = data_pool.get('no_receiver')

    data_pool['sender_new_bal'] = round(sender_old_bal, 8)
    data_pool['receiver_new_bal'] = round(receiver_old_bal, 8)

    if not free_sender:
        data_pool['sender_new_bal'] = round(sender_old_bal - amount_sent, 8)

    if not no_receiver:
        data_pool['receiver_new_bal'] = round(receiver_old_bal + amount_recv, 8)

    return data_pool


def check_banned(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.check_banned
    Fails if either of the users in the transaction are banned
    '''
    assert not bool(data_pool.get('user_sender', {}).get('is_banned', False)), 'Sender is banned'
    assert not bool(data_pool.get('user_receiver', {}).get('is_banned', False)), 'Receiver is banned'
    return data_pool


def check_balances(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.check_banned
    Fails if either of the resultant balances are below zero
    '''
    assert data_pool.get('sender_new_bal') >= 0, 'Sender resulting balance is negative'
    assert data_pool.get('receiver_new_bal') >= 0, 'Receiver resulting balance is negative'
    return data_pool


def clear_tx(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.clear_tx
    Marks a transaction as cleared
    '''
    txid = data_pool.get('txid')
    db = data_pool.get('db')
    db.q_wo('UPDATE transaction SET cleared=1 WHERE txid=%s;', (txid,))
    return data_pool


def update_balance_sender(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.update_balance_sender
    Updates the sender's balance
    '''
    db = data_pool.get('db')
    sender = data_pool.get('sender')
    sender_new_bal = data_pool.get('sender_new_bal')
    sender_old_bal = data_pool.get('sender_old_bal')

    # Avoid a database operation if the balance has not changed
    if sender_new_bal != sender_old_bal:
        db.q_wo('UPDATE user SET balance=%s WHERE username=%s;', (sender_new_bal, sender))

    return data_pool


def update_balance_receiver(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.update_balance_receiver
    Updates the receiver's balance
    '''
    db = data_pool.get('db')
    receiver = data_pool.get('receiver')
    receiver_new_bal = data_pool.get('receiver_new_bal')
    receiver_old_bal = data_pool.get('receiver_old_bal')

    # Avoid a database operation if the balance has not changed
    if receiver_new_bal != receiver_old_bal:
        db.q_wo('UPDATE user SET balance=%s WHERE username=%s;', (receiver_new_bal, receiver))

    return data_pool


def sender_revert_to_old_bal(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.sender_revert_to_old_bal
    Reverts the sender's balance back to its original state
    '''
    db = data_pool.get('db')
    sender = data_pool.get('sender')
    sender_old_bal = data_pool.get('sender_old_bal')

    db.q_wo('UPDATE user SET balance=%s WHERE username=%s;', (sender_old_bal, sender))

    return data_pool


def receiver_revert_to_old_bal(data_pool: dict):
    '''
    grcbot_clearing_house.hooks.clearing_house.receiver_revert_to_old_bal
    Reverts the receiver's balance back to its original state
    '''
    db = data_pool.get('db')
    receiver = data_pool.get('receiver')
    receiver_old_bal = data_pool.get('receiver_old_bal')

    db.q_wo('UPDATE user SET balance=%s WHERE username=%s;', (receiver_old_bal, receiver))

    return data_pool
