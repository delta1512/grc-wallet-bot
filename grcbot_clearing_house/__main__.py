import json
from pathlib import Path

from ryuso import Orchestrator, OrchestratorServer, Hook
import grcbot_clearing_house


# Fetch the RYU spec
spec_path = Path(__file__).parent / 'ryu_spec.json'

with open(spec_path, 'r') as ryu_spec_f:
    ryu_spec = json.load(ryu_spec_f)

# Create the orchestrator
orch = Orchestrator(ryu_spec)

for h in ryu_spec.get('hooks'):
    qual_name = ryu_spec.get('hooks').get(h).get('reference')

    prev_obj = grcbot_clearing_house
    for obj_name in qual_name.split('.')[1:]:
        prev_obj = getattr(prev_obj, obj_name)

    f = prev_obj

    new_hook = Hook(f, qual_name)

    # Now we need to fetch and attach the rectifiers
    for rect in ryu_spec.get('hooks').get(h).get('rectifiers'):
        rect_base_name = rect.split('.')[1]
        rect_qual_name = ryu_spec.get('hooks').get(rect_base_name).get('reference')

        # Fetch the actual function object that runs the rectifier
        prev_obj = grcbot_clearing_house
        for obj_name in rect_qual_name.split('.')[1:]:
            prev_obj = getattr(prev_obj, obj_name)

        f = prev_obj

        rect_hook = Hook(f, rect_qual_name)

        new_hook.add_rectifier(rect_hook)

    orch.add_hook(new_hook)

server = OrchestratorServer(orch)
server.start_server()
