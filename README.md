# GRC-Wallet-Bot

Discord bot by [Delta](https://github.com/delta1512) for the [Gridcoin Discord chat](https://discord.gg/UMWUnMjN4x).

This bot aims to be a third part wallet service for the Gridcoin cryptocurrency and allows people to deposit, store, withdraw, donate and get Gridcoins.

[![DigitalOcean Referral Badge](https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%203.svg)](https://www.digitalocean.com/?refcode=71f8848cef03&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge)

# Recent changes

Recently the GRC Wallet Bot has undergone some drastic changes to conform to [Discord's new standards](https://gist.github.com/Rapptz/4a2f62751b9600a31a0d3c78100287f1) which has caused the main software that this bot uses to interface with discord to go into an unsupported state. These changes also make the GRC wallet bot more modular and open to change (and reduces the amount of spaghetti code I had).

## Movement to an API infrastructure

The Walletbot is now to be run using a private backend API. The entire system will consist of 4 subsystems:
1. The Discord-facing wallet bot. This is the client to the private API and processes user messages.
2. The Private API. This is the system that runs all of the data operations for the walletbot and uses the Discord-facing (or other chat app-facing) as a client. This system uses the RYU sequential orchestrator as a transaction processing backend.
3. [RYU Sequential Orchestrator](https://gitlab.com/delta1512/ryu-sequential-orchestrator) as a transaction processing backend. This is a framework developed by myself to make it really easy to set up sequential processing based on a suite of operations. It will be used to atomically clear transactions on the walletbot.
4. The GRC Wallet Bot backend Client (GWBC). This is a client run privately alongside a Gridcoin wallet that interfaces with the main API server and reports deposits and processes withdrawals. This makes the GRC wallet bot a whole lot more secure as the wallet can now be run privately as opposed to being on a public server.

# Credits

Thanks to the [Gridcoin Foundation](https://github.com/gridcoin-community/Gridcoin-Tasks/issues/248) for choosing to fund the GRC Walletbot, we will see this project continue into the future.
