RULES = '''
1. Do not spam the bot in any way.

2. Do not create duplicate or alternate accounts to maximise faucet or rain collections.

3. Do not mislead other users or attempt to steal the funds of other users through technical or social means.

4. Contact the owner of the bot immediately or post an issue [here](https://gitlab.com/delta1512/grc-wallet-bot/issues) once a bug has been discovered.

5. You must not use the bot to facilitate, participate in or conduct criminal activity.
'''

TERMS = '''
By holding an account with the bot you, the user, agree to the following terms of use:

i. You are fully liable for the movement of your funds. Any transaction that occurs to the wrong person or address is not the responsibility of the owner of the bot.

ii. You are fully liable for all funds stored in the bot. **If in the event your funds were lost for any reason, it is not the responsibility of the owner of the bot to reimburse you.**

iii. You agree to act fairly, considering other users, when using the bot and agree to not abuse the 'generous' functionality such as faucet and rain to your own advantage.

iv. You agree to maintain secrecy where possible in the event that you discover a bug with the bot until the owner is contacted and a resolution found.

v. If you fail to follow any rules outlined by the "rules" command, the owner has every right to ban or remove your account.

vi. If you fail to follow Discord Terms of Service or server rules on any Discord server, the owner has every right to ban or remove your account.

vii. You are fully liable for all legal implications of using this bot for the storage of cryptocurrency assets and any disobedience of the law in any duristiction is not responsibility of the bot's owner.

viii. If any of the above terms are violated, the owner has every right to ban or remove your account.
'''
