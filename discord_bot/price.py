import requests
from time import time
import logging
import json


class PriceClient:
    def __init__(self):
        self.last_updated = 0
        self.last_price = {'usd' : 0, 'btc' : 0}
        self.timeout_sec = 30*60
        self.price_url = 'https://api.coingecko.com/api/v3/coins/gridcoin-research'

    def price(self, btc=False):
        if btc:
            index = 'btc'
        else:
            index = 'usd'

        if round(time()) > self.last_updated + self.timeout_sec:
            try:
                r = requests.get(self.price_url)
                assert r.ok
                data = r.json()
                self.last_price['usd'] = data['market_data']['current_price']['usd']
                self.last_price['btc'] = data['market_data']['current_price']['btc']
            except requests.exceptions.RequestException as E:
                logging.warning('HTTP or connection error when trying to fetch GRC prices: %s', E)
            except AssertionError:
                logging.warning('Price site returned non-200 HTTP status code. Using cached result')
            finally:
                self.last_updated = round(time())
        else:
            logging.debug('Successfully using cached price result')

        return self.last_price[index]

    def conv(self, amt, btc=False):
        return amt * self.price(btc=btc)
