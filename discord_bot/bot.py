import os
import io
import sys
import json
import asyncio
import logging
import time as t # For some reason this was being overridden by a subsequent library
import random as rand
from pathlib import Path
from datetime import datetime
from math import isfinite

import bs4
import qrcode
import requests
from urllib3.exceptions import MaxRetryError
from dotenv import load_dotenv
import disnake
from disnake import Intents, Embed, Colour, File, DMChannel, GroupChannel, TextChannel, CategoryChannel, PartialMessageable, Option, OptionType
from disnake.ext import commands
from disnake.errors import Forbidden
import gwb_swagger_client as swagger_client
from gwb_swagger_client.rest import ApiException

from discord_bot import emotes
from discord_bot import constants
from discord_bot import price as _price


load_dotenv()

intents = Intents.all()
intents.guild_typing = False #pylint: disable=E0237
intents.guild_reactions = False #pylint: disable=E0237
intents.bans = False #pylint: disable=E0237
intents.typing = False #pylint: disable=E0237
intents.reactions = False #pylint: disable=E0237
intents.dm_typing = False #pylint: disable=E0237
intents.dm_reactions = False #pylint: disable=E0237
discord_client = commands.Bot(command_prefix='_', intents=intents)

swag_config = swagger_client.Configuration()
swag_config.api_key['token'] = os.getenv('API_TOKEN_DISCORD')
swag_config.host = os.getenv('GWB_API_URL', 'localhost')

price_client = _price.PriceClient()


def validate_amount(inp, non_neg=False):
    try:
        inp = float(inp)
        if not isfinite(inp) or (non_neg and inp < 0):
            return None
        else:
            return round(inp, 8)
    except (ValueError, OverflowError, TypeError):
        return None


def time_display(tm):
    '''
    Converts an amount of time into it's day, hour, minute, seconds components
    '''
    if tm >= 86400: #1 day
        time_str = t.strftime("%d Days %H Hours %M Minutes %S Seconds", t.gmtime(tm))
        return str(int(time_str[:2])-1) + time_str[2:]
    return t.strftime("%H Hours %M Minutes %S Seconds", t.gmtime(tm))


def get_discord_name(uid):
    try:
        uid = int(uid)
    except ValueError:
        return uid

    for member in discord_client.get_all_members():
        if uid == member.id:
            return member.name

    return uid


async def handle_error(ctx, error, public_only=False):
    logging.debug('Interaction failed with error type %s: %s', type(error), error)

    if isinstance(error, commands.CommandInvokeError):
        error = error.original

    if isinstance(error, commands.CommandOnCooldown):
        return await ctx.send('{}Please wait a few seconds and try again'.format(emotes.INFO))

    if isinstance(error, MaxRetryError):
        return await ctx.send('{}Could not connect to the GRC Wallet Bot service, please try again later'.format(emotes.CONNECTION))

    if not can_send_message_in_channel(ctx.channel, ctx.guild, public_only=public_only):
        allowed_chans = get_allowed_channels(ctx.guild)
        if len(allowed_chans) > 0:
            return await ctx.send('{}Please use the designated channel for this command <#{}>'.format(emotes.CANNOT, allowed_chans[0]))
        else:
            return await ctx.send('{}This command is not available in a private chat'.format(emotes.CANNOT))

    logging.warning('Unexpected interaction error type %s: %s', type(error), error)

    await ctx.send('{}An unexpected error occurred'.format(emotes.ERROR))


def get_allowed_channels(guild):
    final = []

    if not guild is None:
        for chan in guild.channels:
            if len({'bot', 'wallet', 'grc'} & set(chan.name.split('-'))) > 0:
                final.append(chan.id)

    logging.debug('The following are possible allowed channels: %s', final)
    return final


def can_send_message_in_channel(channel, guild, public_only=False):
    logging.debug('Current channel and guild are of the following Disnake types: %s %s', type(channel), type(guild))
    # The bot can only message text channels
    #if not isinstance(channel, TextChannel):
    #    return False

    # Ignore channels that can't be messaged in
    if isinstance(channel, CategoryChannel):
        return False

    # Private channels are ok unless said otherwise
    # It's possible that they are represented as None
    if channel is None or isinstance(channel, (DMChannel, GroupChannel, PartialMessageable)):
        if public_only:
            return False
        return True

    # Set intersect will tell us if there are any words which are the same
    if len({'bot', 'wallet', 'grc'} & set(channel.name.split('-'))) > 0:
        return True

    # If all else fails, search the guild for the appropriate channel
    # If it exists, then fail the interaction
    allowed_chans = get_allowed_channels(guild)
    if len(allowed_chans) > 0:
        return False

    return True


def get_announcement_channels():
    '''
    Return a single channel object for every guild that the bot is attached to.
    The bot will look for a channel containing the terms that lock the bot to
    that channel within the guild, and if it can't find it, then it will simply
    not provide a channel for that guild.
    '''
    servers = discord_client.guilds
    final = []

    for s in servers:
        for c in s.channels:
            if can_send_message_in_channel(c, s, public_only=True):
                final.append(c)
                break

    return final


def limit_to_channel(public_only=False):
    '''
    Runs the wrapped function only if the current channel name is contains "grc"
    or "bot" or "wallet". If the channel doesn't contain those words, the bot
    will attempt to search for a channel with that critera. If there is one,
    then the bot will send a message, redirecting the user to that channel,
    else it will simply run the command.

    If this fails, then it is handled in the interaction failure callback.

    :param public_only: When set to true, will fail if in DM channel, else
        normal rules exist.
    '''
    def limit_to_channel_inner_wrapper(ctx):
        return can_send_message_in_channel(ctx.channel, ctx.guild, public_only=public_only)

    return commands.check(limit_to_channel_inner_wrapper)


def sender_is_admin():
    '''
    Runs the wrapped function only if the user is the admin of the bot.
    Else returns an error message.
    '''
    def sender_is_admin_inner_wrapper(ctx):
        return ctx.author.id == int(os.getenv('ADMIN_UID_DISCORD', 0))

    return commands.check(sender_is_admin_inner_wrapper)


@discord_client.event
async def on_ready():
    game = disnake.Game(name='/help', start=datetime.strptime('18/03/2018', '%d/%M/%Y'))
    await discord_client.change_presence(status=disnake.Status.online, activity=game)


@discord_client.slash_command(
    name='moon',
    description='Get a highly inaccurate prediction of when the GRC price will exceed the ATH'
)
async def moon(ctx):
    clock = ':clock{}:'.format(rand.randint(1, 12))
    day = '{}{}'.format(rand.choice(emotes.NUMS[:3]), rand.choice(emotes.NUMS[1:]))
    month0 = rand.choice(emotes.NUMS[:2])
    month1 = rand.choice(emotes.NUMS[:3]) if emotes.NUMS.index(month0) == 1 else rand.choice(emotes.NUMS)
    month = month0 + month1
    year = ':two::zero:{}{}'.format(rand.choice(emotes.NUMS[2:]), rand.choice(emotes.NUMS))
    await ctx.response.defer()
    await asyncio.sleep(rand.randint(1, 20))
    await ctx.followup.send('{}So when will we moon? Exactly on this date {} {}  {} / {} / {}'.format(
            emotes.CHART_UP, clock, emotes.ARR_RIGHT, day, month, year))


@discord_client.slash_command(
    name='spoon',
    description='Show all the different services that Gridcoin offers'
)
async def spoon(ctx):
    embed = Embed(
        title='Services Provided On Our Network :spoon:',
        colour=Colour.purple()
    )

    embed.add_field(
        name='Gridcoin Wallets',
        value='''
        [Official Wallet](https://gridcoin.us/)
        [Discord Wallet Bot](https://gitlab.com/delta1512/grc-wallet-bot)
        [HolyTransaction](https://holytransaction.com/)
        '''
    )

    embed.add_field(
        name='Block Explorers',
        value='''
        [gridcoin.network](https://gridcoin.network/)
        [Gridcoinstats](https://gridcoinstats.eu/)
        '''
    )

    embed.add_field(
        name='Faucets',
        value='''
        [Discord Wallet Bot](https://gitlab.com/delta1512/grc-wallet-bot)
        [Gridcoin.ch](https://gridcoin.ch/)
        [Gridcoinstats](https://gridcoinstats.eu/)
        [Free Gridcoin](https://freegridco.in/)
        '''
    )

    embed.add_field(
        name='Exchanges',
        value='''
        [Southxchange](https://main.southxchange.com/Market/Book/GRC/BTC)
        [Flyp.me](https://flyp.me/)
        [Txbit.io](https://txbit.io/Asset/GRC)
        '''
    )

    embed.add_field(
        name='Misc',
        value='''
        [Gridcoin Stamper](https://stamp.gridcoin.club/)
        [Free Gridcoin](https://freegridco.in/)
        '''
    )

    await ctx.send(embed=embed)


@discord_client.slash_command(
    name='invite',
    description='Get an invite to the GRC discord server'
)
async def invite(ctx):
    await ctx.send('{}Click here to join the Official GRC Discord server {} {}'.format(
        emotes.CELEBRATE, emotes.ARR_RIGHT, 'https://discord.gg/UMWUnMjN4x'
    ))


@discord_client.slash_command(
    name='price',
    description='Gets the current GRC price in BTC and USD'
)
async def price(ctx):
    embed = Embed(
        title='Current Price of GRC',
        description='USD `${:.4f}`\nBTC `{:.9f}`'.format(
            price_client.price(btc=False),
            price_client.price(btc=True)
        ),
        colour=Colour.green()
    )
    await ctx.send(embed=embed)


@discord_client.slash_command(
    name='conv',
    description='Converts GRC to a USD and BTC amount',
    options=[Option('amount', 'The amount of GRC to estimate stake from', OptionType.number, required=True)]
)
async def conv(ctx, amount):
    embed = Embed(
        title='{} GRC can be converted to...'.format(amount),
        description='USD `${:.4f}`\nBTC `{:.8f}`'.format(
            price_client.conv(amount, btc=False),
            price_client.conv(amount, btc=True)
        ),
        colour=Colour.orange()
    )
    await ctx.send(embed=embed)


@commands.cooldown(1, 10, commands.BucketType.guild)
@discord_client.slash_command(
    name='etts',
    description='Get an estimated time to stake for an amount of GRC.',
    options=[Option('amount', 'The amount of GRC to estimate stake from', OptionType.number, required=True)]
)
async def etts(ctx, amount):
    r = requests.get('https://node.gridcoin.network/API')

    if r.ok:
        resp = r.json()
        supply = resp.get('MoneySupply')
        diff = resp.get('difficulty')

        logging.debug('Block information: %s', resp)

        if not resp.get('error', None) is None:
            await ctx.send('{}Something went wrong, could not fetch blockchain info.'.format(emotes.ERROR))
    else:
        await ctx.send('{}Something went wrong, could not fetch blockchain info.'.format(emotes.ERROR))

    if 0.0125 <= amount <= supply:
        etts = (16000 / amount) * diff
        await ctx.send('{}It is **estimated** that `{} GRC` will stake in `{} days`.'.format(
            emotes.MONEY, amount, round(etts, 2)
        ))
    else:
        await ctx.send('{}An invalid value was provided'.format(emotes.CANNOT))


@commands.cooldown(1, 10, commands.BucketType.guild)
@discord_client.slash_command(
    name='superblock',
    description='Gets a list of the projects and their GRC RAC from the latest superblock',
    options=[]
)
async def superblock(ctx):
    await ctx.response.defer()

    r = requests.get('https://www.gridcoinstats.eu/project')

    # Gets the project-gridcoin RAC pairs for all projects listed on Gridcoinstats
    if r.ok:
        projects = {}
        bs = bs4.BeautifulSoup(r.text, 'html.parser')
        table_rows = bs.body.find_all('tr')

        for tr in table_rows:
            data = tr.find_all('td')
            projects[data[0].text] = data[-2].text.replace(' ', '')
    else:
        return await ctx.followup.send('{}Something went wrong, could not fetch blockchain info.'.format(emotes.ERROR))

    embed = Embed(
        title='Whitelisted projects in the latest superblock',
        description='',
        colour=Colour.green()
    )

    for p in projects:
        embed.description += '**{}**    -    `{}`\n'.format(p, projects.get(p))

    embed.set_footer(text='This service uses gridcoinstats.eu')

    await ctx.followup.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(name='help')
async def help(ctx):
    await ctx.send('{}Type `/` into the chat to see a list of commands\n\n{}To limit me to a single channel, simply make a channel with "bot", "grc" or "wallet" in the name\n\n{}Invite me to your server by clicking on my profile!'.format(
        emotes.INFO, emotes.SETTINGS, emotes.HELLO
    ))


@sender_is_admin()
@discord_client.slash_command(
    name='announce',
    description='Announce a message to all discord servers that have this bot in them',
    options=[Option('text', 'The text to announce', OptionType.string, required=True)]
)
async def announce(ctx, text):
    msgs_success = 0

    await ctx.response.defer()

    for c in get_announcement_channels():
        logging.info('Sending an announcement in %s', c.guild.name)
        try:
            embed = Embed(
                title='An Announcement from the Wallet Bot Owner',
                description=text,
                colour=Colour.red()
            )
            embed.set_footer(text='To restrict these messages see /help')
            await c.send(embed=embed)
            msgs_success += 1
        except Forbidden:
            logging.debug('Failed to send announcement in channel %s in guild %s', c.name, c.guild.name)
        except AttributeError:
            logging.warning('Channel type %s does not support sending for announcements', type(c))
        await asyncio.sleep(1)

    await ctx.followup.send('{}Successfully announced in {}/{} guilds'.format(
        emotes.GOOD, msgs_success, len(discord_client.guilds)
    ))


@limit_to_channel()
@discord_client.slash_command(
    name='qr',
    description='Create a QR code from some text',
    options=[Option('text', 'The text to turn into a QR code', OptionType.string, required=True)]
)
async def qr_code(ctx, text):
    if len(text) > 750:
        await ctx.send('{}You sent too much text to make a QR code'.format(emotes.CANNOT))

    logging.debug('Starting QR code conversion')

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=2
    )
    qr.add_data(text)
    qr.make(fit=True)
    savedir = io.BytesIO()

    logging.debug('Starting to make image')
    img = qr.make_image(fill_color='#5c00b3', back_color='white')
    logging.debug('saving...')
    img.save(savedir, format="PNG")
    savedir.seek(0)

    logging.debug('sending...')

    await ctx.send(file=File(savedir, filename='{}.png'.format(ctx.author.id)))


@limit_to_channel()
@discord_client.slash_command(
    name='rules',
    description='Read the rules for using the walletbot'
)
async def rules(ctx):
    embed = Embed(
        title='GRC Wallet Bot Rules',
        description=constants.RULES,
        url='https://gitlab.com/delta1512/grc-wallet-bot/',
        colour=Colour.purple()
    )

    embed.set_footer(text='Disobeying these rules can result in a permanent ban from using the wallet bot.')

    await ctx.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(
    name='terms',
    description='Read the rules for using the walletbot'
)
async def terms(ctx):
    embed = Embed(
        title='GRC Wallet Bot Rules',
        description=constants.TERMS,
        url='https://gitlab.com/delta1512/grc-wallet-bot/',
        colour=Colour.purple()
    )

    await ctx.send(embed=embed)


@sender_is_admin()
@discord_client.slash_command(
    name='add_advertisement',
    description='(ADMIN ONLY) - Adds a new advertisement to the faucet adverts',
    options=[
        Option('advertisement', 'The advertisement to post', OptionType.string, required=True),
        Option('expiry', 'The number of seconds the advertisement will stay active for', OptionType.integer, required=False)
    ]
)
async def add_advertisement(ctx, advertisement, expiry=None):
    logging.info('Command add_advertisement executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.add_advertisement(advertisement, expiry=expiry)
        logging.debug('Done API request (add_advertisement) and response was: %s', resp)
        embed = Embed(title='Successfully added the advertisement', colour=Colour.green())
    except ApiException as e:
        logging.error('Exception when calling add_advertisement: %s\n', e)
        embed = Embed(title='Failed to add the advertisement', colour=Colour.red())

    await ctx.send(embed=embed)


@sender_is_admin()
@discord_client.slash_command(
    name='ban_user',
    description='(ADMIN ONLY) Bans/unbans a user from using the bot',
    options=[Option('user', 'The user to ban/unban', OptionType.user, required=True)]
)
async def ban_user(ctx, user):
    logging.info('Command ban_user executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(user.id)

    try:
        resp = api.ban_user(username)
        logging.debug('Done API request (ban_user) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling ban_user: %s\n', e)
        embed = Embed(title='Failed to ban the user', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(title='Ban toggled on user {}'.format(user.id), colour=Colour.green())
    embed.add_field(name='Total ban status', value='Banned' if resp.get('total') else 'Not Banned')
    embed.add_field(name='Next faucet time', value=str(resp.get('faucet', 0)))

    await ctx.send(embed=embed)


@sender_is_admin()
@discord_client.slash_command(
    name='faucet_ban_user',
    description='(ADMIN ONLY) Bans a user from using the faucet for a period of time',
    options=[
        Option('user', 'The UID to ban', OptionType.user, required=True),
        Option('ban_time', 'The time (in seconds) offset from UNIX_NOW to ban the user for', OptionType.integer, required=True)
    ]
)
async def faucet_ban_user(ctx, user, ban_time):
    logging.info('Command faucet_ban_user executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(user.id)

    try:
        resp = api.faucetban_user(username, ban_time)
        logging.debug('Done API request (ban_user) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling ban_user: %s\n', e)
        embed = Embed(title='Failed to ban the user', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(title='User Successfully Banned', colour=Colour.green())
    embed.add_field(name='Total ban status', value='Banned' if resp.get('total') else 'Not Banned')
    embed.add_field(name='Next faucet time', value=str(resp.get('faucet', 0)))

    await ctx.send(embed=embed)


@commands.cooldown(1, 10, commands.BucketType.guild)
@discord_client.slash_command(
    name='block',
    description='Gets the current or historical block height from gridcoin.network',
    options=[Option('height', 'The height to query', OptionType.integer, required=False)]
)
async def block(ctx, height=None):
    logging.info('Command block executed by: %s from guild %s', ctx.author.id, ctx.guild)

    if height is None:
        r = requests.get('https://node.gridcoin.network/API')
        c = Colour.green()
    else:
        height = int(validate_amount(height, non_neg=True))

        r = requests.get('https://node.gridcoin.network/API?q=blockbyheight&height={}'.format(height))
        c = Colour.orange()

    if r.ok:
        resp = r.json()
        height = resp.get('height')

        logging.debug('Block information: %s', resp)

        if not resp.get('error', None) is None:
            return await ctx.send('{}Something went wrong, could not fetch block info.'.format(emotes.ERROR))
    else:
        return await ctx.send('{}Something went wrong, could not fetch block info.'.format(emotes.ERROR))

    embed = Embed(title='Information for block #{}'.format(height), colour=c)
    embed.add_field(name='Hash', value=resp.get('hash'))
    embed.add_field(name='Time', value=datetime.utcfromtimestamp(resp.get('time')).strftime('**%Y-%m-%d %H:%M**'))
    embed.add_field(name='Difficulty', value=round(resp.get('difficulty'), 2))
    embed.add_field(name='Net Weight', value=int(resp.get('netstakeweight')))
    embed.add_field(name='No. of Txs', value=len(resp.get('tx', [])))
    embed.add_field(name='Amount Minted', value=round(resp.get('mint', 0), 3))
    embed.add_field(name='Superblock?', value='Yes' if resp.get('IsSuperBlock', False) else 'No')
    embed.set_footer(text='This service uses gridcoin.network')

    await ctx.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(
    name='communications',
    description='Toggles your DM communication settings'
)
async def communications(ctx):
    return await ctx.send('{}This command is under construction, check back later'.format(emotes.INFO))
    logging.info('Command communications executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.communications(username)
        logging.debug('Done API request (communications) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling communications: %s\n', e)
        embed = Embed(title='Failed to change communication settings', colour=Colour.red())
        return await ctx.send(embed=embed)

    await ctx.send(str(resp))


@limit_to_channel(public_only=True)
@commands.cooldown(1, 1, commands.BucketType.user)
@discord_client.slash_command(
    name='get',
    description='Get free GRC!'
)
async def faucet(ctx):
    logging.info('Command faucet executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.faucet(username)
        logging.debug('Done API request (faucet) and response was: %s', resp)
    except ApiException as e:
        try:
            if e.status == 500:
                resp = None
            else:
                resp = json.loads(e.body.decode())
        except (json.JSONDecodeError, AttributeError):
            resp = None

        logging.error('Exception when calling faucet: %s\n', e)

        if isinstance(resp, dict) and 'error' in resp:
            embed = Embed(title=resp.get('error'), colour=Colour.red())
        else:
            embed = Embed(title='Failed to request GRC from the faucet. Try again later.', colour=Colour.red())

        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(
        title='Faucet claim successful!',
        description='You claimed `{} GRC` and your new balance is `{} GRC`'.format(
            resp.get('amount'),
            resp.get('new_balance')
        ),
        colour=Colour.green()
    )

    await ctx.send(embed=embed)

    await asyncio.sleep(1)

    await ctx.send(resp.get('message'))


@sender_is_admin()
@discord_client.slash_command(
    name='get_advertisements',
    description='(ADMIN ONLY) - Fetch and display all currently running faucet advertisements'
)
async def get_advertisements(ctx):
    logging.info('Command get_advertisements executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.get_advertisements()
        logging.debug('Done API request (get_advertisements) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling get_advertisements: %s\n', e)

    await ctx.send(str(resp))


@discord_client.slash_command(
    name='leaderboard',
    description='Shows who are the best donators'
)
async def get_leaderboard(ctx):
    logging.info('Command get_leaderboard executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.get_leaderboard()
        logging.debug('Done API request (get_leaderboard) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling get_leaderboard: %s\n', e)
        embed = Embed(title='Failed to get the leaderboard', colour=Colour.red())
        return await ctx.send(embed=embed)

    final = ''
    resp = resp.to_dict()

    for rank in resp.get('leaderboard', []):
        final += '{}. {} (`{} GRC`)\n'.format(
            rank.get('rank'),
            get_discord_name(rank.get('username')),
            rank.get('amount')
        )

    embed = Embed(
        title='Top 10 donators',
        description=final,
        colour=Colour.purple()
    )

    await ctx.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(
    name='balance',
    description='Gets your current and pending GRC balance as well as deposit address'
)
async def balance(ctx):
    logging.info('Command balance executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.get_user(username)
        logging.debug('Done API request (get_user) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling get_user: %s\n', e)
        embed = Embed(title='Failed to fetch your user information', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    total_value = price_client.conv(resp.get('balance', 0) + resp.get('reserved', 0))
    embed = Embed(title='Your Balance', colour=Colour.purple())
    embed.add_field(name='Usable Balance', value='`{} GRC`'.format(resp.get('balance')))
    embed.add_field(name='Pending Balance', value='`{} GRC`'.format(resp.get('reserved', 0.0)))
    embed.add_field(name='Deposit Address', value='`{}`'.format(resp.get('address')))
    embed.add_field(name='Net Worth', value='USD `${:.2f}`'.format(total_value))
    embed.set_footer(text='The minimum deposit amount is {} GRC'.format(os.getenv('MIN_DEPOSIT', 0)))

    await ctx.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(
    name='stake',
    description='Shows how much GRC you have staked'
)
async def stake(ctx):
    logging.info('Command stake executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.get_user(username)
        logging.debug('Done API request (get_user) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling get_user: %s\n', e)
        embed = Embed(title='Failed to fetch your user information', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(
        title='Your Stakes',
        description='You have staked a total of `{} GRC`'.format(round(resp.get('stakes', 0.0), 4)),
        colour=Colour.purple()
    )
    embed.set_footer(text='To stake, you need to have at least {} GRC in your balance. Staking occurs automatically.'.format(os.getenv('MIN_STAKE_BAL', 0.0125)))

    await ctx.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(
    name='time',
    description='Gets the next time until you are able to use the faucet.'
)
async def time(ctx):
    logging.info('Command address executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.get_user(username)
        logging.debug('Done API request (get_user) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling get_user: %s\n', e)
        embed = Embed(title='Failed to fetch your user information', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    next_time = resp.get('next_faucet_time')
    current_time = int(t.time())

    if next_time > current_time:
        delta = next_time - current_time
        await ctx.send('{}You can claim the faucet in `{}`'.format(emotes.TIME, time_display(delta)))
    else:
        await ctx.send('{}You can claim the faucet!'.format(emotes.GOOD))


@limit_to_channel()
@discord_client.slash_command(
    name='address',
    description='Gets your current GRC address that you can use for deposits'
)
async def address(ctx):
    logging.info('Command address executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.get_user(username)
        logging.debug('Done API request (get_user) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling get_user: %s\n', e)
        embed = Embed(title='Failed to fetch your user information', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()

    await ctx.send('**The minimum deposit amount is `{} GRC`**'.format(os.getenv('MIN_DEPOSIT', 0)))
    await asyncio.sleep(0.75)
    await ctx.send(resp.get('address'))


@limit_to_channel()
@discord_client.slash_command(
    name='history',
    description='Gets your most recent transaction history',
    options=[Option('page', 'The page of transactions to query (default 1)', OptionType.integer, required=False)]
)
async def history(ctx, page=1):
    logging.info('Command history executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)
    page = max(page - 1, 0)

    await ctx.response.defer()

    try:
        resp = api.history(username, page=page)
        logging.debug('Done API request (history) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling history: %s\n', e)
        embed = Embed(title='Failed to fetch your transaction history', colour=Colour.red())
        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    message = ''

    for tx in resp.get('history', []):
        time_executed = datetime.utcfromtimestamp(tx.get('time_executed')).strftime('**%Y-%m-%d %H:%M**')
        message += '(**{}**) `{} GRC` from {} to {} at {}\n\n'.format(
            tx.get('tx_type', 'basic').title(),
            tx.get('amount'),
            tx.get('sender'),
            tx.get('receiver'),
            time_executed
        )

    embed = Embed(
        title='Showing page {} of transactions in your account (UTC time)'.format(page + 1),
        description=message,
        colour=Colour.purple()
    )

    await ctx.followup.send(embed=embed)


@discord_client.slash_command(
    name='rain',
    description='Rain an amount of GRC over every user on the walletbot',
    options=[
        Option('amount', 'The amount of GRC to rain', OptionType.number, required=True)
    ]
)
async def rain(ctx, amount):
    logging.info('Command rain executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    MAX_RAIN = float(os.getenv('MAX_RAIN', 1000))
    if amount > MAX_RAIN:
        return await ctx.send('{}The amount to rain is too high, maximum is `{} GRC`'.format(emotes.CANNOT, MAX_RAIN))

    try:
        await ctx.response.defer()
        resp = api.rain(username, amount)
        logging.debug('Done API request (rain) and response was: %s', resp)
    except ApiException as e:
        try:
            if e.status == 500:
                resp = None
            else:
                resp = json.loads(e.body.decode())
        except (json.JSONDecodeError, AttributeError):
            resp = None

        logging.error('Exception when calling rain: %s\n', e)

        if isinstance(resp, dict) and 'error' in resp:
            embed = Embed(title=resp.get('error'), colour=Colour.red())
        else:
            embed = Embed(title='Failed to create a rain. Try again later.', colour=Colour.red())

        return await ctx.followup.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(
        title='Rain successful!',
        description=resp.get('message'),
        colour=Colour.green()
    )

    await ctx.followup.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(name='register', description='Create an account on the GRC Wallet Bot')
async def register_user(ctx):
    logging.info('Command register_user executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)
    accept_terms = True

    try:
        resp = api.register_user(username, accept_terms)
        logging.debug('Done API request (register_user) and response was: %s', resp)
    except ApiException as e:
        try:
            if e.status == 500:
                resp = None
            else:
                resp = json.loads(e.body.decode())
        except (json.JSONDecodeError, AttributeError):
            resp = None

        logging.error('Exception when calling transaction: %s\n', e)

        if isinstance(resp, dict) and 'error' in resp:
            embed = Embed(title=resp.get('error'), colour=Colour.red())
        else:
            embed = Embed(title='Failed to create your user account, try again later', colour=Colour.red())

        return await ctx.send(embed=embed)

    await ctx.send('{}Welcome to the GRC Walletbot! **By using the bot you agree to the terms and conditions** (/terms and /rules).\nYour GRC address is `{}`'.format(emotes.CELEBRATE, resp.to_dict().get('address')))


@limit_to_channel()
@discord_client.slash_command(
    name='status',
    description='Gets the status of the GRC Wallet Bot, staking and faucet'
)
async def server_info(ctx):
    logging.info('Command server_info executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.server_info()
        logging.debug('Done API request (server_info) and response was: %s', resp)
    except ApiException as e:
        logging.error('Exception when calling server_info: %s\n', e)

    resp = resp.to_dict()
    embed = Embed(title='GRC Wallet Bot Status', colour=Colour.green())
    embed.add_field(name='Min Transfer', value='`{} GRC`'.format(resp.get('min_transfer')))
    embed.add_field(name='Withdraw Fee', value='`{} GRC`'.format(resp.get('withdraw_fee')))
    embed.add_field(name='Deposit Confirmations', value='{}'.format(int(resp.get('confirmations'))))
    embed.add_field(name='Faucet Timeout', value='{} hours'.format(resp.get('faucet_time')))
    embed.add_field(name='Faucet Balance', value='`{} GRC`'.format(round(resp.get('faucet_funds'), 2)))
    embed.add_field(name='Users', value='{}'.format(resp.get('users')))
    embed.add_field(name='Transactions', value='{}'.format(int(resp.get('tx_vol'))))
    embed.add_field(name='Donations', value='`{} GRC`'.format(round(resp.get('donation_vol'), 2)))

    await ctx.send(embed=embed)


@discord_client.slash_command(
    name='give',
    description='Transfer GRC to someone else',
    options=[
        Option('user', 'The @-mention of the recipient', OptionType.user, required=True),
        Option('amount', 'The amount of GRC to transfer', OptionType.number, required=True)
    ]
)
async def transaction(ctx, user, amount: float):
    logging.info('Command transaction executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    sender = str(ctx.author.id)
    receiver = str(user.id)

    if sender == receiver:
        return await ctx.send('{}Cannot send GRC to yourself'.format(emotes.CANNOT))

    try:
        resp = api.transaction(sender, receiver, amount)
        logging.debug('Done API request (transaction) and response was: %s', resp)
    except ApiException as e:
        try:
            if e.status == 500:
                resp = None
            else:
                resp = json.loads(e.body.decode())
        except (json.JSONDecodeError, AttributeError):
            resp = None

        logging.error('Exception when calling transaction: %s\n', e)

        if isinstance(resp, dict) and 'error' in resp:
            embed = Embed(title=resp.get('error'), colour=Colour.red())
        else:
            embed = Embed(title='Failed to send GRC. Try again later.', colour=Colour.red())

        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(
        title='Transaction Successful!',
        description='You sent `{} GRC`'.format(resp.get('amount')),
        color=Colour.green()
    )
    embed.set_footer(text='GRC Bot TXID {}'.format(resp.get('txid')))

    await ctx.send(embed=embed)


@discord_client.slash_command(
    name='fgive',
    description='Donate some GRC to the faucet',
    options=[
        Option('amount', 'The amount of GRC to transfer', OptionType.number, required=True)
    ]
)
async def fgive(ctx, amount: float):
    logging.info('Command fgive executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    sender = str(ctx.author.id)
    receiver = 'FAUCET'

    try:
        resp = api.transaction(sender, receiver, amount)
        logging.debug('Done API request (transaction) and response was: %s', resp)
    except ApiException as e:
        try:
            if e.status == 500:
                resp = None
            else:
                resp = json.loads(e.body.decode())
        except (json.JSONDecodeError, AttributeError):
            resp = None

        logging.error('Exception when calling transaction: %s\n', e)

        if isinstance(resp, dict) and 'error' in resp:
            embed = Embed(title=resp.get('error'), colour=Colour.red())
        else:
            embed = Embed(title='Failed to send GRC. Try again later.', colour=Colour.red())

        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(
        title='Transaction Successful!',
        description='You donated `{} GRC`\n{}Thanks for your contribution!'.format(
            resp.get('amount'),
            emotes.HEART
        ),
        color=Colour.green()
    )
    embed.set_footer(text='GRC Bot TXID {}'.format(resp.get('txid')))

    await ctx.send(embed=embed)


@limit_to_channel()
@discord_client.slash_command(
    name='withdraw',
    description='Send GRC out of the wallet bot via the blockchain',
    options=[
        Option('address', 'The GRC address to send GRC to on the blockchain', OptionType.string, required=True),
        Option('amount', 'The amount of GRC to withdraw. This the value before the fee is subtracted', OptionType.number, required=True)
    ]
)
async def withdraw(ctx, address, amount):
    logging.info('Command withdraw executed by: %s from guild %s', ctx.author.id, ctx.guild)

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))
    username = str(ctx.author.id)

    try:
        resp = api.withdraw(username, address, amount)
        logging.debug('Done API request (withdraw) and response was: %s', resp)
    except ApiException as e:
        try:
            if e.status == 500:
                resp = None
            else:
                resp = json.loads(e.body.decode())
        except (json.JSONDecodeError, AttributeError):
            resp = None

        logging.error('Exception when calling withdraw: %s\n', e)

        if isinstance(resp, dict) and 'error' in resp:
            embed = Embed(title=resp.get('error'), colour=Colour.red())
        else:
            embed = Embed(title='Failed to withdraw GRC. Try again later.', colour=Colour.red())

        return await ctx.send(embed=embed)

    resp = resp.to_dict()
    embed = Embed(
        title='Transaction Successful!',
        description='You withdrew `{} GRC`'.format(resp.get('amount')),
        color=Colour.green()
    )
    embed.set_footer(text='Your withdrawal will be sent over the blockchain in the next 5min-10min')

    await ctx.send(embed=embed)


# Error handlers on a per-function basis. This is because on_command_error and
# similar commands didn't work for some reason.
@withdraw.error
async def withdraw_error(ctx, error):
    await handle_error(ctx, error)

@server_info.error
async def server_info_error(ctx, error):
    await handle_error(ctx, error)

@register_user.error
async def register_user_error(ctx, error):
    await handle_error(ctx, error)

@address.error
async def address_error(ctx, error):
    await handle_error(ctx, error)

@history.error
async def history_error(ctx, error):
    await handle_error(ctx, error)

@balance.error
async def balance_error(ctx, error):
    await handle_error(ctx, error)

@communications.error
async def communications_error(ctx, error):
    await handle_error(ctx, error)

@terms.error
async def terms_error(ctx, error):
    await handle_error(ctx, error)

@rules.error
async def rules_error(ctx, error):
    await handle_error(ctx, error)

@qr_code.error
async def qr_code_error(ctx, error):
    await handle_error(ctx, error)

@help.error
async def help_error(ctx, error):
    await handle_error(ctx, error)

@stake.error
async def stake_error(ctx, error):
    await handle_error(ctx, error)

@etts.error
async def etts_error(ctx, error):
    await handle_error(ctx, error)

@block.error
async def block_error(ctx, error):
    await handle_error(ctx, error)

@superblock.error
async def superblock_error(ctx, error):
    await handle_error(ctx, error)


### MAIN
if __name__ == '__main__':
    discord_client.run(os.getenv('DISCORD_SECRET'))
