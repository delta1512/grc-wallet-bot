# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class Advertisement(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, expiry: int=None, text: str=None, adid: int=None):  # noqa: E501
        """Advertisement - a model defined in Swagger

        :param expiry: The expiry of this Advertisement.  # noqa: E501
        :type expiry: int
        :param text: The text of this Advertisement.  # noqa: E501
        :type text: str
        :param adid: The adid of this Advertisement.  # noqa: E501
        :type adid: int
        """
        self.swagger_types = {
            'expiry': int,
            'text': str,
            'adid': int
        }

        self.attribute_map = {
            'expiry': 'expiry',
            'text': 'text',
            'adid': 'adid'
        }
        self._expiry = expiry
        self._text = text
        self._adid = adid

    @classmethod
    def from_dict(cls, dikt) -> 'Advertisement':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The advertisement of this Advertisement.  # noqa: E501
        :rtype: Advertisement
        """
        return util.deserialize_model(dikt, cls)

    @property
    def expiry(self) -> int:
        """Gets the expiry of this Advertisement.


        :return: The expiry of this Advertisement.
        :rtype: int
        """
        return self._expiry

    @expiry.setter
    def expiry(self, expiry: int):
        """Sets the expiry of this Advertisement.


        :param expiry: The expiry of this Advertisement.
        :type expiry: int
        """

        self._expiry = expiry

    @property
    def text(self) -> str:
        """Gets the text of this Advertisement.


        :return: The text of this Advertisement.
        :rtype: str
        """
        return self._text

    @text.setter
    def text(self, text: str):
        """Sets the text of this Advertisement.


        :param text: The text of this Advertisement.
        :type text: str
        """

        self._text = text

    @property
    def adid(self) -> int:
        """Gets the adid of this Advertisement.


        :return: The adid of this Advertisement.
        :rtype: int
        """
        return self._adid

    @adid.setter
    def adid(self, adid: int):
        """Sets the adid of this Advertisement.


        :param adid: The adid of this Advertisement.
        :type adid: int
        """

        self._adid = adid
