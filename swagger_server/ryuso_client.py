import json
import socket
import logging

from swagger_server import CLEARANCE_SERVER_IP, CLEARANCE_SERVER_PORT


def clear_transaction(txid, program='process_transaction'):
    logging.debug('Preparing transaction %s for clearance', txid)
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as chs:
            chs.settimeout(5)
            chs.connect((CLEARANCE_SERVER_IP, CLEARANCE_SERVER_PORT))

            logging.debug('Successfully connected to clearance server')

            task = {'program' : program, 'data_pool' : {'txid' : txid}}

            send_str = json.dumps(task)

            logging.debug('Sending task to clearance house: %s', task)

            chs.sendall(send_str.encode())

            logging.debug('Successfully sent the task to the server and awaiting response')

            data, _adata, _flags, _addr = chs.recvmsg(1000000) #1MB

            logging.debug('Raw data received from the server: %s', data)
    except json.JSONDecodeError:
        logging.error('Bad JSON received from clearing house: %s', data)
        return None
    except OSError as e:
        logging.error('Failed to communicate with clearing server: %s', e)
        return None
    finally:
        chs.close()

    return json.loads(data.decode())
