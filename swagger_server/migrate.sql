USE grcbot; -- Old db

-- Clean the data
UPDATE udb SET balance=0 WHERE balance < 0 OR balance IS NULL;
UPDATE udb SET donations=0 WHERE donations < 0 OR donations IS NULL;
UPDATE udb SET last_faucet=0 WHERE last_faucet < 0 OR last_faucet IS NULL;
INSERT INTO tx_log SELECT * FROM tx_log_bak;


USE grcbot_new;

-- Migrate all users
INSERT INTO user
SELECT uid, address, ROUND(balance, 8), donations, UNIX_TIMESTAMP(), 0, last_faucet
FROM grcbot.udb
WHERE uid != 'FAUCET' and uid != 'RAIN';

-- Migrate ban statuses
UPDATE user SET is_banned=1
WHERE username IN
(SELECT u.uid FROM grcbot.udb u JOIN grcbot.blacklist b ON u.uid=b.uid);

-- Migrate the advertisements
INSERT INTO advertisement SELECT NULL, message, 9999999999 FROM grcbot.adverts WHERE active=1;

-- Migrate donors
INSERT INTO charity SELECT name, address, name FROM grcbot.donors;

-- Migrate the faucet transactions only
INSERT INTO transaction
SELECT UUID(), _time, 1, sender, receiver, ROUND(amount, 8), ROUND(amount, 8), 0, 9999, NULL, 1
FROM grcbot.tx_log
WHERE sender='FAUCET' AND amount > 0;

-- Migrate the stake transactions only
INSERT INTO transaction
SELECT UUID(), _time, 7, 'DEPOSIT', receiver, ROUND(amount, 8), ROUND(amount, 8), 0, 9999, NULL, 1
FROM grcbot.tx_log
WHERE sender='STAKE' AND amount > 0;

-- Migrate the deposits
INSERT INTO transaction
SELECT UUID(), _time, 6, 'DEPOSIT', receiver, ROUND(amount, 8), ROUND(amount, 8), 0, 9999, NULL, 1
FROM grcbot.tx_log
WHERE sender='DEPOSIT' AND amount > 0;

-- Migrate the rains
INSERT INTO transaction
SELECT UUID(), _time, 8, 'FAUCET', receiver, ROUND(amount, 8), ROUND(amount, 8), 0, 9999, NULL, 1
FROM grcbot.tx_log
WHERE sender='RAIN' AND amount > 0;

-- Migrate regular transactions
INSERT INTO transaction
SELECT UUID(), _time, 0, sender, receiver, ROUND(amount, 8), ROUND(amount, 8), 0, 9999, NULL, 1
FROM grcbot.tx_log
WHERE sender!='RAIN' AND sender!='DEPOSIT' AND sender!='FAUCET'
  AND sender!='STAKE' AND amount > 0
  AND sender IN (SELECT username FROM user)
  AND receiver IN (SELECT username FROM user);
