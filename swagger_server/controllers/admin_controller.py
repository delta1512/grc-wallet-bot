import time

import connexion
import six

from swagger_server.models.ban_status import BanStatus  # noqa: E501

from swagger_server import queries as q
from swagger_server import validation as v


def add_advertisement(advertisement, expiry=None):  # noqa: E501
    """Submits a new faucet advertisement

     # noqa: E501

    :param advertisement: The text of the advertisement. Maximum of 255 characters
    :type advertisement: str
    :param expiry: The expiry date of the advertisement. Default is never.
    :type expiry: str

    :rtype: DefaultError
    """
    if expiry is None:
        expiry = q.FOREVER

    expiry = int(v.validate_amount(expiry, non_neg=True))

    if expiry is None:
        return {'error' : 'Invalid expiry provided'}, 400

    expiry += int(time.time())

    q.add_advertisement(advertisement, expiry)

    return {}, 200


def ban_user(username):  # noqa: E501
    """Toggles the ban on a user from using the platform

     # noqa: E501

    :param username: The name of the user to ban/unban. For Discord, this is their UID.
    :type username: str

    :rtype: BanStatus
    """
    db = q.dbConn()

    if q.is_user(username, db=db):
        stat = q.get_ban_status(username, db=db)
        q.ban_user(username, not bool(stat.get('total')), db=db)
        stat = q.get_ban_status(username, db=db)
        stat = BanStatus(stat.get('total'), stat.get('faucet'))
    else:
        stat = BanStatus(1, 0)

    return stat


def faucetban_user(username, time):  # noqa: E501
    """Toggles the ban on a user from using the faucet

     # noqa: E501

    :param username: The name of the user to ban/unban. For Discord, this is their UID.
    :type username: str

    :rtype: BanStatus
    """
    db = q.dbConn()
    if q.is_user(username, db=db):
        q.faucetban_user(username, v.validate_amount(time), db=db)

    status = q.get_ban_status(username, db=db)
    return BanStatus(status.get('total', 0), status.get('faucet', 0))


def get_advertisements():  # noqa: E501
    """Shows all of the currently running advertisements and their IDs

     # noqa: E501


    :rtype: Advertisements
    """
    return 'do some magic!'
