import logging
import os

from connexion.exceptions import OAuthProblem
from typing import List
"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""
def check_backend_token(api_key, required_scopes):
    backend_token = os.getenv('BACKEND_API_TOKEN', None)

    assert not backend_token is None, 'You must define static tokens in .env'

    logging.debug('Authorising connection with key: %s', api_key)

    if api_key == backend_token:
        return {'auth_success' : True}
    else:
        logging.warning('Invalid access token provided: %s', api_key)
        raise OAuthProblem('Access denied')


def check_token(api_key, required_scopes):
    tokens = os.getenv('API_TOKENS', None)

    assert not tokens is None, 'You must define static tokens in .env'

    logging.debug('Authorising connection with key: %s', api_key)

    if api_key in tokens.split(','):
        logging.info('Access granted to token: %s', api_key)
        return {'auth_success' : True}
    else:
        logging.warning('Invalid access token provided: %s', api_key)
        raise OAuthProblem('Access denied')
