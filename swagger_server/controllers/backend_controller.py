import connexion
import six
import logging

from swagger_server.models.deposit import Deposit  # noqa: E501
from swagger_server.models.addresses import Addresses  # noqa: E501
from swagger_server.models.default_error import DefaultError  # noqa: E501
from swagger_server.models.addresses_with_balances import AddressesWithBalances  # noqa: E501
from swagger_server.models.addresses_with_balances_addresses_with_balances import AddressesWithBalancesAddressesWithBalances
from swagger_server.models.withdrawal_success import WithdrawalSuccess  # noqa: E501
from swagger_server.models.withdrawals import Withdrawals  # noqa: E501
from swagger_server.models.withdrawal import Withdrawal  # noqa: E501

from swagger_server import queries as q
from swagger_server import validation as v
from swagger_server.ryuso_client import clear_transaction
from swagger_server import MIN_STAKE_BAL, DEPOSIT_CONFIRMATIONS


def apply_balance_changes(addresses, changes, is_stake):  # noqa: E501
    """Applies the provided offsets to the balances of each addressholder

    (For backend functionality only) # noqa: E501

    :param addresses:
    :type addresses: List[str]
    :param changes:
    :type changes: List[float]
    :param is_stake: Whether the balance changes are indicative of a stake
    :type is_stake: str

    :rtype: DefaultError
    """
    if connexion.request.is_json:
        payload = connexion.request.get_json()
        addresses = payload.get('addresses', addresses)
        changes = payload.get('changes', changes)
        is_stake = payload.get('is_stake', is_stake)

    db = q.dbConn()
    recv_users = [q.address_to_username(a) for a in addresses]
    tx_type = 7 if is_stake else 6
    tx_batch = q.apply_balance_changes(
        'DEPOSIT',
        recv_users,
        changes,
        tx_type=tx_type,
        db=db
    )

    for tx in tx_batch:
        result = clear_transaction(tx)

        if result is None:
            logging.error('Failed to contact the clearing house when applying balance changes for %s', tx)
        elif not result.get('success', True):
            logging.error('Clearing house refused to apply balance changes for %s', tx)
        else:
            logging.debug('Successfully applied balance changes for %s', tx)

    return {}, 200


def get_registered_addresses():  # noqa: E501
    """Lists all active wallet addresses on the system

    (For backend functionality only) # noqa: E501


    :rtype: Addresses
    """
    return Addresses(q.get_all_addresses())


def get_registered_addresses_with_balances():  # noqa: E501
    """Lists all active wallet addresses on the system

    (For backend functionality only) # noqa: E501


    :rtype: AddressesWithBalances
    """
    addr_bal_objs = []
    addr_bal_pairs = q.get_all_addresses_with_balance(min_bal=MIN_STAKE_BAL)

    for abp in addr_bal_pairs:
        if v.validate_address(abp[0]):
            addr_bal_objs.append(AddressesWithBalancesAddressesWithBalances(abp[0], round(abp[1], 8)))

    return AddressesWithBalances(addr_bal_objs)


def push_new_addresses(addresses):  # noqa: E501
    """Adds new addresses to the bank of addresses used for new users

    (For backend functionality only) # noqa: E501

    :param addresses:
    :type addresses: List[str]

    :rtype: DefaultError
    """
    db = q.dbConn()

    for addr in addresses:
        q.add_address_to_bank(addr, db=db)

    return {}, 200


def sync_pending_deposits(deposits=None):  # noqa: E501
    """Sends all tx info for deposits that are currently being tracked

    (For backend functionality only) This is so that we can tell users there are pending deposits # noqa: E501

    :param addresses:
    :type addresses: list | bytes

    :rtype: DefaultError
    """
    if connexion.request.is_json:
        deposits = [d for d in connexion.request.get_json().get('deposits', [])]  # noqa: E501


    db = q.dbConn()

    for d in deposits:
        logging.debug('Processing deposit: %s', d)

        transaction = q.get_tx_for_deposit(d.get('txid'), db=db)

        if not bool(transaction):
            logging.debug('Deposit is new, creating new transaction')
            # If the transaction doesn't exist, then create one
            recipient = q.address_to_username(d.get('address'), db=db)
            if q.can_transact('DEPOSIT', recipient, d.get('amount'), 5, db=db):
                q.create_transaction('DEPOSIT', recipient, d.get('amount'), confirmations=d.get('confirmations'), tx_type=5, meta=d.get('txid'), db=db)

            transaction = q.get_tx_for_deposit(d.get('txid'), db=db)

        # If the transaction exists, then update confirmations if they have changed
        if transaction.get('confirmations') < d.get('confirmations'):
            q.update_tx_confirmations(transaction.get('txid'), d.get('confirmations'), db=db)

        # If the transaction is confirmed, send it off for clearance
        if d.get('confirmations') >= DEPOSIT_CONFIRMATIONS:
            logging.debug('Deposit is fully confirmed, sending to RYUSO')
            q.change_tx_type(transaction.get('txid'), 6, db=db)
            logging.info('Deposit fully cleared: %s', transaction.get('txid'))
            clear_transaction(transaction.get('txid'))

    return {}, 200


def get_withdrawals():  # noqa: E501
    """Gets all of the withdrawals that need to be processed

    (For backend functionality only) # noqa: E501

    :rtype: Withdrawals
    """
    wdr_objs = []
    pend_withdrawals = q.get_pending_withdrawals()

    for pw in pend_withdrawals:
        wdr_objs.append(Withdrawal(pw[0], pw[1], round(pw[2], 8)))

    return Withdrawals(wdr_objs)


def update_withdrawal_success(withdrawals=[]):  # noqa: E501
    """Submit a list of withdrawals that were successfully cleared

    (For backend functionality only) This is so that we can clear pending withdrawals # noqa: E501

    :param withdrawals:
    :type withdrawals: list | bytes

    :rtype: DefaultError
    """
    if connexion.request.is_json:
        withdrawals = [d for d in connexion.request.get_json().get('withdrawals', [])]

    for wdr in withdrawals:
        txid = wdr.get('txid')

        if wdr.get('success', False):
            logging.info('Withdrawal %s successfully completed', txid)
            q.change_tx_type(txid, 3)
        else:
            logging.warning('Withdrawal failed: %s', txid)
            q.change_tx_type(txid, 4)

    return {}, 200
