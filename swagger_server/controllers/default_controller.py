import os
import time
import json
import socket
import logging
import random as r

import connexion
import six

from swagger_server.models.advertisements import Advertisements  # noqa: E501
from swagger_server.models.ban_status import BanStatus  # noqa: E501
from swagger_server.models.block_info import BlockInfo  # noqa: E501
from swagger_server.models.communication_preferences import CommunicationPreferences  # noqa: E501
from swagger_server.models.default_error import DefaultError  # noqa: E501
from swagger_server.models.leaderboard import Leaderboard  # noqa: E501
from swagger_server.models.leaderboard_entry import LeaderboardEntry  # noqa: E501
from swagger_server.models.new_user_success import NewUserSuccess  # noqa: E501
from swagger_server.models.server_info import ServerInfo  # noqa: E501
from swagger_server.models.transaction import Transaction
from swagger_server.models.transaction_history import TransactionHistory  # noqa: E501
from swagger_server.models.transaction_success import TransactionSuccess  # noqa: E501
from swagger_server.models.user import User  # noqa: E501
from swagger_server import util

from swagger_server import queries as q
from swagger_server import validation as v
from swagger_server.ryuso_client import clear_transaction
from swagger_server import FCT_TIMEOUT_HRS, FCT_MIN, FCT_MAX, MIN_RAIN, LOG_LEVEL, WITHDRAWAL_FEE

advert_cycle = 0


def time_display(tm):
    '''
    Converts an amount of time into it's day, hour, minute, seconds components
    '''
    if tm >= 86400: #1 day
        time_str = time.strftime("%d Days %H Hours %M Minutes %S Seconds", time.gmtime(tm))
        return str(int(time_str[:2])-1) + time_str[2:]
    return time.strftime("%H Hours %M Minutes %S Seconds", time.gmtime(tm))


def restrict_functionality(is_user: bool=False, not_banned: bool=False):
    '''
    A decorator that returns a HTTP error based on particular criteria.

    This function expects the username of the user to be the first arg or a
    member of the kwargs.

    :param is_user: Enforces a user that exists if set to true
    :param not_banned: Enforces a user that is not banned if set to true
    '''
    def restrict_functionality_outer_wrapper(func):
        def restrict_functionality_inner_wrapper(*args, user=None, token_info=None, body=None, **kwargs):
            # Get user from args
            username = kwargs.get('username', kwargs.get('sender', None))


            if username is None:
                assert len(args) > 0, 'Programming error: function does not expect a username'
                username = args[0]

            if is_user:
                if not q.is_user(username):
                    return {'error' : 'You must have an account to use this command. Try /register'}, 400

            if not_banned:
                stat = q.get_ban_status(username)

                if stat.get('total', True):
                    return {'error' : 'You are banned from using the Wallet Bot'}, 403

            return func(*args, **kwargs)
        return restrict_functionality_inner_wrapper
    return restrict_functionality_outer_wrapper


def communications(username):  # noqa: E501
    """Toggles communication preferences

    Toggles direct communications on or off and returns the result # noqa: E501

    :param username: The name of the user changing their preferences. For Discord, this is their UID.
    :type username: str

    :rtype: CommunicationPreferences
    """
    return 'do some magic!'


@restrict_functionality(is_user=True, not_banned=True)
def faucet(username):  # noqa: E501
    """Performs a faucet claim for the provided user

     # noqa: E501

    :param username: The name of the user claiming. For Discord, this is their UID.
    :type username: str

    :rtype: TransactionSuccess
    """
    global advert_cycle

    receiver = username
    amount = round(r.uniform(FCT_MIN, FCT_MAX), 8)
    db = q.dbConn()
    remaining_faucet_time = q.get_time_until_next_faucet(username, db=db)

    if remaining_faucet_time >= 0:
        return {'error' : 'You must wait {}'.format(time_display(remaining_faucet_time))}, 400

    can_tx, reason = q.can_transact('FAUCET', receiver, amount, tx_type=1, db=db)

    if can_tx:
        txid = q.create_transaction('FAUCET', receiver, amount, tx_type=1, db=db)
        result = clear_transaction(txid)

        if result is None or result.get('_program_fail', False): # success only appears when something is wrong, hence default True
            logging.error('Faucet transaction %s failed to clear.', txid)
            # Mark the transaction as failed and cleared
            q.fail_transaction(txid)
            return {'error' : 'Server failed to clear the transaction'}, 410 # 410 For the fun of it lol
    else:
        logging.debug('Failed to execute faucet transaction because reason: %s', reason)

        # Make errors be more informative at the client-side
        if 'insufficient' in reason:
            reason = 'Could not make a faucet claim, the faucet has run out of funds.'

        return {'error' : reason}, 400

    logging.debug('Faucet transaction %s successfully completed with result %s', txid, result)

    advert = q.get_faucet_advert(advert_cycle, db=db)
    advert_cycle += 1

    # A trick to record the last faucet time as now
    q.faucetban_user(username, 0, db=db)

    return TransactionSuccess(amount, txid, result.get('receiver_new_bal'), advert)


def get_leaderboard():  # noqa: E501
    """Shows the people who have donated the most GRC

     # noqa: E501

    :rtype: Leaderboard
    """
    leaderboard = q.get_donation_leaderboard()
    entries = []

    for rank in leaderboard:
        entries.append(LeaderboardEntry(
            rank.get('rank'),
            rank.get('username'),
            rank.get('amount')
        ))

    return Leaderboard(entries)


@restrict_functionality(is_user=True, not_banned=True)
def get_user(username):  # noqa: E501
    """Gets the user's current profile information

     # noqa: E501

    :param username: The name of the user. For Discord, this is their UID.
    :type username: str

    :rtype: User
    """
    db = q.dbConn()
    user_info = q.get_user(username, db=db)
    reserved_bals = q.get_reserved_bals(username, db=db)
    # Subtract pending outgoing transactions from the balance.
    active_balance = user_info.get('balance') - reserved_bals[1]
    dm_enabled = False # STUB
    stakes = q.get_total_amount_staked(username, db=db)

    # Sometimes it will return None
    if not bool(stakes):
        stakes = 0.0

    next_faucet_time = int(user_info.get('last_faucet', 0) + FCT_TIMEOUT_HRS * q.HOUR)

    return User(
        username, user_info.get('address'), active_balance,
        sum(reserved_bals), user_info.get('donations'), dm_enabled,
        stakes, next_faucet_time
    )


@restrict_functionality(is_user=True)
def history(username, page=0):  # noqa: E501
    """Shows the transaction history of an account

     # noqa: E501

    :param username: The name of the user changing their preferences. For Discord, this is their UID.
    :type username: str
    :param page: The page of history to examine.
    :type page: int

    :rtype: TransactionHistory
    """
    txs = q.get_transaction_history(username, page=page)
    final = []

    for tx in txs:
        if tx.get('sender') == username:
            sender = 'YOU'
            receiver = tx.get('receiver_name')
            amount = tx.get('amount_sent')
        else:
            sender = tx.get('sender_name')
            receiver = 'YOU'
            amount = tx.get('amount_recv')

        final.append(Transaction(
            tx.get('txid'),
            tx.get('type_name'),
            sender,
            receiver,
            amount,
            tx.get('time_executed')
        ))

    return TransactionHistory(final)


@restrict_functionality(is_user=True, not_banned=True)
def rain(sender, amount):  # noqa: E501
    """Splits the provided amount amongst everyone in the database

     # noqa: E501

    :param amount:
    :type amount: float

    :rtype: TransactionSuccess
    """
    amount = v.validate_amount(amount, non_neg=True)

    if amount is None:
        logging.debug('Rain failed from bad amount')
        return {'error' : 'Invalid amount to rain'}, 400

    if amount < MIN_RAIN:
        logging.debug('Rain failed because lower than minimum rain')
        return {
            'error' : 'Amount too small to rain, you must rain at least `{} GRC`'.format(MIN_RAIN)
        }, 400

    db = q.dbConn()

    can_tx, reason = q.can_transact(sender, 'FAUCET', amount, tx_type=8, db=db)

    if can_tx:
        # We rain on all the database members who are actual users
        # so we have to filter out the non-user accounts
        rain_recipients = set(filter(
            # This is some trickery with the fact that discord IDs are all numbers
            lambda u: (not v.validate_amount(u) is None) and (u != sender),
            q.get_all_unbanned_users()
        ))

        if len(rain_recipients) < 3:
            logging.debug('Rain failed because not enough users')
            return {'error' : 'Not enough people to rain on'}, 400

        n_rain_recipients = len(rain_recipients)
        rain_amt = round(amount / n_rain_recipients, 8)
        rain_amts = [rain_amt] * n_rain_recipients
        rain_sum = 0

        logging.info('Raining %s GRC on %s users, initiated by %s', amount, n_rain_recipients, sender)

        batch = q.apply_balance_changes(sender, list(rain_recipients), rain_amts, tx_type=8, db=db)
        logging.debug('Batch of rain transactions created, they are:\n%s', batch)

        for txid in batch:
            result = clear_transaction(txid)
            logging.debug('Rain clearance result: %s', result)

            if result is None or result.get('_program_fail', False): # TODO: Can be replaced with ryuso.util.has_failed when the package has been updated
                logging.warning('Rain transaction %s failed with result: %s', txid, result)
                q.fail_transaction(txid)
            else:
                rain_sum += rain_amt

        # Rains are donations
        q.apply_donation(sender, rain_sum, db=db)

        return {
            'message' : 'Successfully rained {} GRC on {} people!'.format(amount, n_rain_recipients)
        }, 200
    else:
        return {'error' : reason}, 400


def register_user(username, accept_terms):  # noqa: E501
    """Registers a user on the system

     # noqa: E501

    :param username: The name of the user. For Discord, this is their UID.
    :type username: str
    :param accept_terms: Whether the user has accepted the terms and privacy policy
    :type accept_terms: bool

    :rtype: NewUserSuccess
    """
    if not accept_terms:
        return {'error' : 'The terms have not been accepted, please register again and accept the terms'}, 400

    if q.is_user(username):
        return {'error' : 'Cannot make another account, you have already signed-up'}, 400

    if v.validate(username, 'username'):
        result = q.register_user(username)
        return NewUserSuccess(result)
    else:
        return {'error' : 'Invalid username provided'}, 400


def server_info():  # noqa: E501
    """Lists all constants, config parameters and statistics for the system

     # noqa: E501

    :rtype: ServerInfo
    """
    db = q.dbConn()
    return ServerInfo(
        0, # MIN WITHDRAW might not actually be used
        WITHDRAWAL_FEE,
        os.getenv('MIN_TRANSFER', 0.0125),
        os.getenv('DEPOSIT_CONFIRMATIONS', 1),
        os.getenv('FCT_TIMEOUT_HRS', 1),
        q.get_total_users(db=db),
        q.get_n_txs(db=db),
        q.get_total_donations(db=db),
        0, #q.get_last_stake_time(db=db), Deprecated because too long to query
        q.get_user('FAUCET', db=db).get('balance', 0)
    )


@restrict_functionality(is_user=True, not_banned=True)
def transaction(sender, receiver, amount):  # noqa: E501
    """Sends a transaction to another user

    You can specify a user either by name or uid # noqa: E501

    :param sender: The username of the user sending. For Discord, this is their UID.
    :type sender: str
    :param receiver: The username of the user receiving. For Discord, this is their UID.
    :type receiver: str
    :param amount:
    :type amount: float

    :rtype: TransactionSuccess
    """
    amount = v.validate_amount(amount, non_neg=True)
    db = q.dbConn()
    can_tx, reason = q.can_transact(sender, receiver, amount, tx_type=0, db=db)

    if can_tx:
        txid = q.create_transaction(sender, receiver, amount, tx_type=0, db=db)
        result = clear_transaction(txid)

        if result is None or result.get('_program_fail', False):
            logging.error('Transaction %s failed to clear.', txid)
            q.fail_transaction(txid)
            return {'error' : 'Server failed to clear the transaction'}, 410 # 410 For the fun of it lol

        if receiver == 'FAUCET':
            # If the receiver is the faucet, then it is a donation
            q.apply_donation(sender, amount, db=db)
    else:
        logging.debug('User failed to execute transaction because reason: %s', reason)
        return {'error' : reason}, 400

    logging.debug('Transaction %s successfully completed with result %s', txid, result)
    return TransactionSuccess(amount, txid, result.get('sender_new_bal'), '')


@restrict_functionality(is_user=True, not_banned=True)
def withdraw(username, address, amount):  # noqa: E501
    """Makes a withdrawal from the GRC Wallet

     # noqa: E501

    :param username: The name of the user sending. For Discord, this is their UID.
    :type username: str
    :param address:
    :type address: str
    :param amount:
    :type amount: float

    :rtype: TransactionSuccess
    """
    sender = username
    amount = v.validate_amount(amount, non_neg=True)

    if not v.validate_address(address) and LOG_LEVEL > logging.DEBUG:
        return {'error' : 'Provided address is invalid'}, 400

    db = q.dbConn()
    can_tx, reason = q.can_transact(sender, 'WITHDRAW', amount, tx_type=2, db=db)

    if can_tx:
        txid = q.create_transaction(
            sender,
            'WITHDRAW',
            amount,
            tx_type=2,
            meta=address,
            db=db
        )
        result = clear_transaction(txid)

        if result is None or result.get('_program_fail', False):
            logging.error('Transaction %s failed to clear', txid)
            q.fail_transaction(txid)
            return {'error' : 'Server failed to clear the transaction'}, 410 # 410 For the fun of it lol
    else:
        logging.debug('User failed to execute transaction because reason: %s', reason)
        return {'error' : reason}, 400

    logging.debug('Transaction %s succeffully completed with result %s', txid, result)
    return TransactionSuccess(amount - WITHDRAWAL_FEE, txid, result.get('sender_new_bal'), '')
