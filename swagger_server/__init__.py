import os
import logging
from logging.handlers import RotatingFileHandler

from dotenv import load_dotenv

load_dotenv()

FCT_TIMEOUT_HRS = float(os.getenv('FCT_TIMEOUT_HRS', 1))
FCT_MIN = float(os.getenv('FCT_MIN', 0.00125))
FCT_MAX = float(os.getenv('FCT_MAX', 0.0125))
MIN_RAIN = float(os.getenv('MIN_RAIN', 100))
MAX_RAIN = float(os.getenv('MAX_RAIN', 1000))
MAX_WITHDRAW = float(os.getenv('MAX_WITHDRAW', 1000000))
CLEARANCE_SERVER_IP = os.getenv('CLEARANCE_SERVER_IP', 'localhost')
CLEARANCE_SERVER_PORT = int(os.getenv('CLEARANCE_SERVER_PORT'))
PAGE_LEN = int(os.getenv('PAGE_LEN', 5))
LOG_LEVEL = int(os.getenv('LOG_LEVEL', logging.DEBUG))
WITHDRAWAL_FEE = float(os.getenv('WITHDRAWAL_FEE', 0))
MIN_STAKE_BAL = float(os.getenv('MIN_STAKE_BAL', 0))
MIN_TRANSFER = float(os.getenv('MIN_TRANSFER', 0.00005))
DEPOSIT_CONFIRMATIONS = int(os.getenv('DEPOSIT_CONFIRMATIONS'))


logging.basicConfig(
    level=int(os.getenv('LOG_LEVEL', logging.DEBUG)),
    format=os.getenv('LOG_FMT'),
    datefmt=os.getenv('LOG_DATE_FMT'),
    handlers=[
        RotatingFileHandler(
            os.path.join(os.getenv('LOG_DIR'), 'api_walletbot.log'),
            maxBytes=int(os.getenv('LOG_MAX_BYTES_PER_FILE', logging.DEBUG)),
            backupCount=int(os.getenv('LOG_COUNT', logging.DEBUG))
        )
    ]
)
