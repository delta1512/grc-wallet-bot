import re
import json
from math import isfinite
from datetime import datetime as d


### REGEX RULES
RULES = {
    'username' : '''^[\d]{3,20}$''',
    # TEMP: Remove the testnet address prefixes
    'address' : '''^(([mn][a-zA-Z0-9])|(b[a-zA-Z])|(S[1-9ABCDEFGHJKLM])|(R[wxyz]))[a-zA-Z0-9]{32}$'''
}


### CONSTANTS
# Immutable fields for the whole database (updated as needed)
# Immutability here refers to preventing the user or admin from updating these fields directly
IMMUTABLE_FIELDS = ()
###


def enforce_type(val, want_type, retval=None):
    '''
    Ensures a value is of a given type, else it returns a constant value.
    To trigger an exception instead of returning, pass an exception object
    as retval.
    '''
    if type(val) is want_type:
        return val
    else:
        if isinstance(retval, Exception):
            raise retval
        else:
            return retval


def validate(field: str, data: str, rule_set=RULES):
    if data is None or data == 'None':
        test_data = ''
    else:
        test_data = str(data)

    if field in rule_set:
        final = bool(re.match(rule_set[field], test_data))

        return final
    else:
        # If a particular field doesn't have any rules, it is assumed valid
        return True


def validate_address(address: str):
    return validate('address', address)


def validate_amount(inp, non_neg=False):
    try:
        inp = float(inp)
        if not isfinite(inp) or (non_neg and inp < 0):
            return None
        else:
            return round(inp, 8)
    except (ValueError, OverflowError, TypeError):
        return None


def str_to_bool(s):
    '''
    A special helper function that converts a string to a boolean.
    This is because "False" and "0" is True
    '''
    s = str(s)

    try:
        return bool(int(s))
    except ValueError:
        pass

    if s.lower() == 'true':
        return True
    elif s.lower() == 'false':
        return False
    else:
        return bool(s)


def sanitise_order_by(val: str, template: dict, default: str=None):
    '''
    Sanitises an SQL ORDER BY field for direct insertion into an SQL string.
    The template of the SQL table must be provided to check against.
    The if-statement below will be lazy-loaded with exceptions as they are found.
    '''
    val = str(val)
    final = default

    if val in template:
        final = val

    # There are certain exceptions in the naming scheme
    # which need to be corrected so that SQL works
    if final == 'time':
        final = '_time'

    return final


def sanitise_order_by_dir(val: str, default: str=None):
    val = str(val).upper()
    final = default

    if val in ['ASC', 'DESC']:
        final = val

    return final
