CREATE SCHEMA IF NOT EXISTS grcbot_new;

USE grcbot_new;


-- Addresses are to be accumulated and stored in this table until needed
-- To be deleted as they are used
CREATE TABLE IF NOT EXISTS address_bank (
  address         CHAR(34),

  PRIMARY KEY (address)
);


CREATE TABLE IF NOT EXISTS user (
  username      CHAR(32),
  address       CHAR(34),
  balance       DOUBLE UNSIGNED DEFAULT 0 NOT NULL,
  donations     DOUBLE UNSIGNED DEFAULT 0 NOT NULL,
  time_created  BIGINT UNSIGNED NOT NULL,
  is_banned     BOOLEAN DEFAULT 0 NOT NULL,
  last_faucet   BIGINT UNSIGNED DEFAULT 0 NOT NULL,

  PRIMARY KEY (username)
);


CREATE TABLE IF NOT EXISTS setting (
  setting_id      INT UNSIGNED NOT NULL,
  name            CHAR(50),
  type            CHAR(10),               -- Python type
  default_val     TINYTEXT NOT NULL,
  read_only       BOOLEAN DEFAULT 0 NOT NULL,

  PRIMARY KEY (setting_id)
);


CREATE TABLE IF NOT EXISTS user_setting (
  username        CHAR(32),
  setting_id      INT UNSIGNED NOT NULL,
  value           TEXT NOT NULL,

  PRIMARY KEY (username, setting_id),
  FOREIGN KEY (username) REFERENCES user (username)
);


CREATE TABLE IF NOT EXISTS advertisement (
  id              INT UNSIGNED NOT NULL AUTO_INCREMENT,
  message         TINYTEXT,
  expiry          BIGINT UNSIGNED NOT NULL,

  PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS charity (
  name    CHAR(50),
  address CHAR(34),
  source  TINYTEXT,

  PRIMARY KEY (name)
);


CREATE TABLE IF NOT EXISTS tx_type (
  type_id         TINYINT UNSIGNED NOT NULL,
  type_name       CHAR(30) NOT NULL,
  description     TEXT(1000),
  fee             DOUBLE UNSIGNED NOT NULL,
  free_sender     BOOLEAN DEFAULT 0 NOT NULL, -- Whether sender pays nothing (for deposits)
  no_receiver     BOOLEAN DEFAULT 0 NOT NULL, -- Whether the receiver gets nothing (for withdrawals)

  PRIMARY KEY (type_id)
);


CREATE TABLE IF NOT EXISTS transaction (
  txid            CHAR(64) NOT NULL,
  _time           BIGINT UNSIGNED NOT NULL,
  type            TINYINT UNSIGNED NOT NULL,
  sender          CHAR(32) NOT NULL,
  receiver        CHAR(32) NOT NULL,
  amount_sent     DOUBLE UNSIGNED NOT NULL,
  amount_recv     DOUBLE UNSIGNED NOT NULL,
  fee             DOUBLE UNSIGNED NOT NULL,
  confirmations   INT UNSIGNED DEFAULT 99999 NOT NULL,
  meta            TINYTEXT,
  cleared         BOOLEAN DEFAULT 0 NOT NULL,

  PRIMARY KEY (txid),
  FOREIGN KEY (sender)              REFERENCES user      (username),
  FOREIGN KEY (receiver)            REFERENCES user      (username),
  FOREIGN KEY (type)                REFERENCES tx_type   (type_id)
);


CREATE TABLE IF NOT EXISTS hit (
  id              BIGINT UNSIGNED AUTO_INCREMENT,
  _time           BIGINT UNSIGNED NOT NULL,
  server_id       BIGINT UNSIGNED,
  ip              CHAR(39),
  endpoint        CHAR(32),

  PRIMARY KEY (id)
);


INSERT INTO user VALUES ('DEPOSIT', 'DUMMY1', DEFAULT, DEFAULT, UNIX_TIMESTAMP(), DEFAULT, DEFAULT);
INSERT INTO user VALUES ('WITHDRAW', 'DUMMY1', DEFAULT, DEFAULT, UNIX_TIMESTAMP(), DEFAULT, DEFAULT);
INSERT INTO user VALUES ('FAUCET', 'DUMMY1', DEFAULT, DEFAULT, UNIX_TIMESTAMP(), DEFAULT, DEFAULT);
INSERT INTO user VALUES ('RAIN', 'DUMMY1', DEFAULT, DEFAULT, UNIX_TIMESTAMP(), DEFAULT, DEFAULT);

INSERT INTO setting VALUES (1000, 'Allow the sending of DM notifications', 'bool', '1', 0);
INSERT INTO setting VALUES (1998, 'Agreement to privacy policy', 'bool', '1', 1);
INSERT INTO setting VALUES (1999, 'Agreement to terms and conditions', 'bool', '1', 1);

INSERT INTO tx_type VALUES (0, 'basic', 'one person makes a payment to another person', 0, 0, 0);
INSERT INTO tx_type VALUES (1, 'faucet', 'a payment made from the faucet to a regular user', 0, 0, 0);
INSERT INTO tx_type VALUES (2, 'withdrawal_pending', 'a withdrawal has been created but not committed', 0.25, 0, 1);
INSERT INTO tx_type VALUES (3, 'withrawal', 'a withdrawal has been submitted and committed successfully', 0, 0, 1);
INSERT INTO tx_type VALUES (4, 'withdrawal_failed', 'the submitted withdrawal failed to be committed', 0, 0, 0);
INSERT INTO tx_type VALUES (5, 'deposit_pending', 'a user deposit awaiting confirmation', 0, 1, 0);
INSERT INTO tx_type VALUES (6, 'deposit', 'a user deposit has been completed', 0, 1, 0);
INSERT INTO tx_type VALUES (7, 'stake', 'the walletbot has staked and distributed the currency', 0, 1, 0);
INSERT INTO tx_type VALUES (8, 'rain', 'the walletbot has staked and distributed the currency', 0, 0, 0);
INSERT INTO tx_type VALUES (9, 'failed', 'This transaction failed and no funds were moved', 0, 1, 1);
