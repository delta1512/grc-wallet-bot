#!/usr/bin/env python3

import connexion

from swagger_server import encoder


def main():
    app = connexion.App(__name__, specification_dir='./swagger/', server='tornado', port=5000)
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'GRC Wallet API'}, pythonic_params=True, base_path='/')
    app.run(port=5000)


if __name__ == '__main__':
    main()
