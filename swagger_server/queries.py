import os
import time
import secrets as s

import logging
import mysql.connector as cnx

from swagger_server import validation as v
from swagger_server import FCT_TIMEOUT_HRS, MIN_TRANSFER, MAX_RAIN, MAX_WITHDRAW, PAGE_LEN

HOUR = 60 * 60
DAY = HOUR * 24
YEAR = 365 * DAY
FOREVER = 200 * YEAR

DB_HOST = os.getenv('DB_HOST')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
DB_NAME = os.getenv('DB_NAME')


class dbConn():
    def __init__(self):
        logging.debug('Database connection opened.')
        self.needs_commit = False
        self.db = cnx.connect(
            host=DB_HOST,
            user=DB_USER,
            passwd=DB_PASS,
            db=DB_NAME
        )

    def q_ro(self, query, args=(), one=False):
        cur = self.db.cursor()
        cur.execute(query, args)
        result = cur.fetchall()
        cur.close()
        return (result[0] if result else []) if one else result

    def q_wo(self, query, args=(), commit_now=False):
        cur = self.db.cursor()
        cur.execute(query, args)
        cur.close()

        if commit_now:
            self.db.commit()
        else:
            self.needs_commit = True

    def __del__(self):
        if self.needs_commit:
            self.db.commit()
        if hasattr(self.db, 'close'):
            self.db.close()
            logging.debug('Database connection closed')


def rand_string(length: int, human: bool=False):
    '''
    Returns a fixed length string of `length` characters.
    human - whether to use human-readable characters.
    '''
    final = ''
    while len(final) < length:
        if human:
            final += s.token_hex(64)
        else:
            final += s.token_urlsafe(64)
    return final[:length]


def database_decorator(function):
    def wrapper(*args, **kwargs):
        if not 'db' in kwargs:
            kwargs['db'] = dbConn()
            logging.debug("No database connection was found, injecting one now.")
        return function(*args, **kwargs)
    return wrapper


@database_decorator
def is_user(username, db=None):
    result = db.q_ro('SELECT count(*) FROM user WHERE username=%s;', (username,), one=True)

    return result[0] > 0


@database_decorator
def address_to_username(address, db=None):
    '''Assumes that the user exists'''
    result = db.q_ro('SELECT username FROM user WHERE address=%s;', (address,), one=True)
    return result[0]


@database_decorator
def is_valid_type(tx_type, db=None):
    result = db.q_ro('SELECT count(*) FROM tx_type WHERE type_id=%s;', (tx_type,), one=True)

    return result[0] > 0


@database_decorator
def get_user(username, db=None):
    info = db.q_ro('SELECT * FROM user WHERE username=%s;', (username,), one=True)

    return {
        'username' : info[0],
        'address' : info[1],
        'balance' : info[2],
        'donations' : info[3],
        'time_created' : info[4],
        'is_banned' : info[5],
        'last_faucet' : info[6]
    }


@database_decorator
def get_new_address(db=None):
    address = db.q_ro('SELECT address FROM address_bank LIMIT 1;', one=True)
    assert len(address) > 0, 'No addresses in the bank'
    db.q_wo('DELETE FROM address_bank WHERE address=%s;', (address[0],), commit_now=True)
    return address[0]


@database_decorator
def register_user(username, db=None):
    address = get_new_address(db=db)

    db.q_wo(
        '''INSERT INTO user VALUES (%s, %s, DEFAULT, DEFAULT, %s, DEFAULT, %s);''',
        (username, address, int(time.time()), int(time.time() + 2*DAY))
    )

    return address


@database_decorator
def get_reserved_bals(username, db=None):
    '''
    :returns: A tuple of sum_in and sum_out which represent the sum of pending
        amounts to receive and to send respectively. Those which are outgoing
        should be subtracted from the current balance.
    '''
    pend_in_tx = db.q_ro(
        'SELECT type, amount_sent, amount_recv FROM transaction WHERE cleared=0 AND receiver=%s;',
        (username,)
    )
    pend_out_tx = db.q_ro(
        'SELECT type, amount_sent, amount_recv FROM transaction WHERE cleared=0 AND sender=%s;',
        (username,)
    )

    sum_in = 0
    sum_out = 0

    for tx in pend_in_tx:
        sum_in += tx[2]

    for tx in pend_out_tx:
        sum_out += tx[1]

    return sum_in, sum_out


@database_decorator
def get_total_amount_staked(username, db=None):
    result = db.q_ro(
        'SELECT sum(amount_recv) FROM transaction WHERE type=7 AND receiver=%s;',
        (username,),
        one=True
    )

    return result[0]


@database_decorator
def get_ban_status(username, db=None):
    result = db.q_ro(
        'SELECT is_banned, last_faucet FROM user WHERE username=%s;',
        (username,),
        one=True
    )

    return {
        'total' : int(result[0]),
        'faucet' : result[1]
    }


@database_decorator
def ban_user(username, ban_status, db=None):
    db.q_wo(
        'UPDATE user SET is_banned=%s WHERE username=%s;',
        (ban_status, username),
        commit_now=True
    )


@database_decorator
def get_time_until_next_faucet(username, db=None):
    result = db.q_ro('SELECT last_faucet FROM user WHERE username=%s;', (username,), one=True)
    return int((result[0] + HOUR * FCT_TIMEOUT_HRS) - time.time())


@database_decorator
def faucetban_user(username, ban_time, db=None):
    # Offset the current unix time by the provided ban time
    assert not ban_time is None
    last_faucet_time = int(time.time() + ban_time)

    db.q_wo(
        'UPDATE user SET last_faucet=%s WHERE username=%s;',
        (last_faucet_time, username),
        commit_now=True
    )


@database_decorator
def get_total_donations(db=None):
    result = db.q_ro('SELECT sum(donations) FROM user;', one=True)
    return result[0]


@database_decorator
def get_n_txs(db=None):
    result = db.q_ro(
        'SELECT TABLE_ROWS FROM information_schema.TABLES WHERE TABLE_NAME="transaction" AND TABLE_SCHEMA=%s;',
        (DB_NAME,),
        one=True
    )
    return result[0]


@database_decorator
def get_total_users(db=None):
    result = db.q_ro('SELECT count(*) FROM user;', one=True)
    return result[0]


@database_decorator
def get_all_unbanned_users(db=None):
    results = db.q_ro('SELECT username FROM user WHERE is_banned=0;')
    return [r[0] for r in results] # Unwrap the results


@database_decorator
def get_all_addresses(db=None):
    results = db.q_ro('SELECT address FROM user WHERE is_banned=0;')
    return [r[0] for r in results] # Unwrap the results


@database_decorator
def get_all_addresses_with_balance(min_bal=0, db=None):
    return db.q_ro(
        '''SELECT address, balance FROM user WHERE is_banned=0 AND balance>%s;''',
        (min_bal,)
    )


@database_decorator
def get_donation_leaderboard(db=None):
    result = db.q_ro(
        '''SELECT username, donations FROM user
        ORDER BY donations DESC LIMIT 10;'''
    )

    final = []

    for i, r in enumerate(result):
        final.append({
            'rank' : i + 1,
            'username' : r[0],
            'amount' : r[1]
        })

    return final


@database_decorator
def can_transact(sender: str, receiver: str, amount: float, tx_type: int, db=None):
    '''
    Based on the transaction information, see whether this transaction can be
    executed or not so that it doesn't reach the clearance house.

    :param amount: This is the amount of the transaction, you must run
        v.validate_amount on this value before it enters the function.

    :returns: A (can_tx, reason) tuple where can_tx tells you whether the
        transaction can pass validation and the reason is only set to a
        non-empty value when can_tx is false.
    '''
    # Check the amount validity first
    if amount is None:
        return False, 'Invalid amount provided'

    if amount == 0:
        return False, 'Attempting to create a redundant transaction'

    # Check for redundant sender/receiver
    if sender == receiver:
        return False, 'You cannot send a transaction to yourself'
    
    # There is a maximum amount that people can withdraw
    if tx_type == 2 and amount >= MAX_WITHDRAW:
        return False, 'You are withdrawing too much GRC, maximum withdrawal is `{} GRC`'.format(MAX_WITHDRAW)

    # Check that the type is correct
    if not is_valid_type(tx_type, db=db):
        return False, 'The transaction is of an invalid type'

    # Rains must not exceed the max rain
    if tx_type == 8 and amount > MAX_RAIN:
        return False, 'You are raining too much GRC, the maximum to rain is `{} GRC`'.format(MAX_RAIN)

    # Check that a faucet transaction is always sent from the faucet
    if tx_type == 1 and sender != 'FAUCET':
        return False, 'Sender must be faucet'

    # Only one pending withdrawal can be made
    if tx_type == 2 and has_pending_withdrawal(sender, db=db):
        return False, 'You already have a withdrawal pending, please wait.'

    # Check that the users exist
    if not is_user(sender, db=db):
        return False, 'You don\'t have an account, please create one'

    if not is_user(receiver, db=db):
        return False, 'Recipient doesn\'t exist'

    # Check that the users are not banned
    ban_stat_sender = get_ban_status(sender, db=db)
    if ban_stat_sender.get('total', True):
        return False, 'You are banned from using the walletbot'

    ban_stat_receiver = get_ban_status(receiver, db=db)
    if ban_stat_receiver.get('total', True):
        return False, 'Receiver is banned from using the walletbot'

    # Check sender's available balance
    # All transactions are "receiver-pays-fee" so we need not check the fee here
    user_sender = get_user(sender, db=db)

    if round(user_sender.get('balance') - amount, 8) < 0:
        return False, 'You have insufficient balance to make that transfer'

    # Prevent 0-value transactions from slipping through
    fee = get_fee_from_type(tx_type, db=db)
    net_amount = round(amount - fee, 8)
    if net_amount < MIN_TRANSFER:
        return False, 'You must send more than the minimum amount plus the fees'

    return True, ''


@database_decorator
def create_transaction(sender: str, receiver: str, amount: float, fee: float=None, tx_type: int=0, confirmations: int=99999, meta: str=None, db=None):
    '''
    Create a transaction in the database ready for clearance. After a
    transaction is made using this function, it should be sent to the clearing
    house for clearance.

    :param amount: The amount to be debited. This will be used to create the
        amount_recv by subtracting the fee.
    :param fee: The fee to be charged on the transaction, this is an override to
        the default fee defined by the transaction type.

    :returns: The transaction ID of the successfully created transaction.
    '''
    txid = rand_string(32, human=True)
    current_time = int(time.time())
    amount_sent = round(amount, 8)

    # Calculate amount_recv. Check if custom fee passed-in else, use tx_type
    if fee is None:
        fee = get_fee_from_type(tx_type, db=db)

    amount_recv = round(amount - fee, 8)

    logging.debug(
        'Transaction %s being created with sender=%s, receiver=%s, amount=%s, fee=%s, tx_type=%s, confirmations=%s, amount_sent=%s, amount_recv=%s',
        txid, sender, receiver, amount, fee, tx_type, confirmations, amount_sent, amount_recv
    )

    db.q_wo(
        '''
        INSERT INTO transaction VALUES (
            %s, %s, %s,
            %s, %s, %s,
            %s, %s, %s,
            %s, DEFAULT
        );
        ''',
        (
            txid, current_time, tx_type,
            sender, receiver, amount_sent,
            amount_recv, fee, confirmations,
            meta
        ),
        commit_now=True
    )

    return txid


@database_decorator
def fail_transaction(txid, db=None):
    db.q_wo(
        'UPDATE transaction SET cleared=1, type=9 WHERE txid=%s;',
        (txid,),
        commit_now=True
    )


@database_decorator
def get_faucet_advert(i, db=None):
    results = db.q_ro('SELECT message FROM advertisement WHERE expiry>UNIX_TIMESTAMP();')
    return results[i % len(results)][0]


@database_decorator
def add_advertisement(advertisement, expiry, db=None):
    db.q_wo('INSERT INTO advertisement VALUES (DEFAULT, %s, %s);', (advertisement, expiry))


@database_decorator
def apply_balance_changes(sender, receivers, changes, tx_type=0, db=None):
    '''
    Creates a transaction from the sender to the receiver for each receiver.
    Every receiver is paired with a change at the exact same index. This
    function will batch-create the transactions but not commit them.
    It is recommended to check if the sender can make a single
    transaction with the sum of changes before continuing.
    This function only checks the following:
        - That all changes are non-zero and non-negative

    :param sender: The single sender to send from. This must be a valid user

    :param receivers: A list of usernames of users to receive the changes.

    :param changes: A list of non-negative changes in the balance of each
        receiver. Each change should map to a receiver at the same index.

    :returns: A list of txids that represent the batches that were processed
        and need to be committed via the clearing house.
    '''
    batch = []
    receiver_changes = []

    # Check everything rather than failing in the middle of batch processing
    for i, c in enumerate(changes):
        amount = v.validate_amount(c, non_neg=True)
        if c is None or c == 0:
            return []
        else:
            receiver_changes.append((receivers[i], amount))

    for r, c in receiver_changes:
        batch.append(create_transaction(sender, r, c, tx_type=tx_type, db=db))

    return batch


@database_decorator
def get_fee_from_type(tx_type: int, db=None):
    return db.q_ro('SELECT fee FROM tx_type WHERE type_id=%s;', (tx_type,), one=True)[0]


@database_decorator
def get_transaction_history(username, page=0, db=None):
    final = []
    results = db.q_ro(
        '''SELECT tx.*, txt.type_name, txt.fee FROM transaction tx
        JOIN tx_type txt ON tx.type=txt.type_id
        WHERE sender=%s OR receiver=%s
        ORDER BY _time DESC
        LIMIT %s OFFSET %s;''',
        (username, username, PAGE_LEN, page * PAGE_LEN)
    )

    for tx in results:
        final.append({
            'txid' : tx[0],
            'time_executed' : tx[1],
            'type' : tx[2],
            'sender' : tx[3],
            'receiver' : tx[4],
            'amount_sent' : tx[5],
            'amount_recv' : tx[6],
            'fee' : tx[7],
            'confirmations' : tx[8],
            'meta' : tx[9],
            'cleared' : tx[10],
            'type_name' : tx[11],
            'fee' : tx[12], # TODO: Figure out why this is here
            # A little trickery with user IDs, if the username is a user ID, then
            # don't disclose it to the frontend
            'sender_name' : tx[3] if v.validate_amount(tx[3]) is None else 'USER',
            'receiver_name' : tx[4] if v.validate_amount(tx[4]) is None else 'USER'
        })

    return final


@database_decorator
def add_address_to_bank(address, db=None):
    db.q_wo('INSERT INTO address_bank VALUES (%s);', (address,))


@database_decorator
def get_pending_withdrawals(db=None):
    return db.q_ro(
        '''SELECT txid, meta, amount_recv FROM transaction WHERE type=2 AND cleared=1;'''
    )


@database_decorator
def get_tx_for_deposit(blockchain_txid: str, db=None):
    tx = db.q_ro(
        'SELECT * FROM transaction WHERE meta=%s;',
        (blockchain_txid,),
        one=True
    )

    if len(tx) > 0:
        return {
            'txid' : tx[0],
            'time_executed' : tx[1],
            'type' : tx[2],
            'sender' : tx[3],
            'receiver' : tx[4],
            'amount_sent' : tx[5],
            'amount_recv' : tx[6],
            'fee' : tx[7],
            'confirmations' : tx[8],
            'meta' : tx[9],
            'cleared' : tx[10],
        }
    else:
        return None


@database_decorator
def update_tx_confirmations(txid: str, confirmations: int, db=None):
    db.q_wo(
        'UPDATE transaction SET confirmations=%s WHERE txid=%s;',
        (confirmations, txid),
        commit_now=True
    )


@database_decorator
def change_tx_type(txid: str, new_tx_type: int, db=None):
    db.q_wo(
        'UPDATE transaction SET type=%s WHERE txid=%s;',
        (new_tx_type, txid),
        commit_now=True
    )


@database_decorator
def apply_donation(username: str, amount: float, db=None):
    db.q_wo('UPDATE user SET donations=round(donations+%s, 8) WHERE username=%s;', (amount, username))


@database_decorator
def has_pending_withdrawal(username: str, db=None):
    result = db.q_ro('SELECT count(*) FROM transaction WHERE sender=%s AND type=2;', (username,), one=True)

    return result[0] > 0
