# coding: utf-8

"""
    GRC Wallet API

    The backend system that runs The GRC Wallet Bot and other services  # noqa: E501

    OpenAPI spec version: 0.0.3
    Contact: info@yora.tech
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class WithdrawalSuccess(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'txid': 'str',
        'success': 'bool'
    }

    attribute_map = {
        'txid': 'txid',
        'success': 'success'
    }

    def __init__(self, txid=None, success=None):  # noqa: E501
        """WithdrawalSuccess - a model defined in Swagger"""  # noqa: E501
        self._txid = None
        self._success = None
        self.discriminator = None
        if txid is not None:
            self.txid = txid
        if success is not None:
            self.success = success

    @property
    def txid(self):
        """Gets the txid of this WithdrawalSuccess.  # noqa: E501


        :return: The txid of this WithdrawalSuccess.  # noqa: E501
        :rtype: str
        """
        return self._txid

    @txid.setter
    def txid(self, txid):
        """Sets the txid of this WithdrawalSuccess.


        :param txid: The txid of this WithdrawalSuccess.  # noqa: E501
        :type: str
        """

        self._txid = txid

    @property
    def success(self):
        """Gets the success of this WithdrawalSuccess.  # noqa: E501


        :return: The success of this WithdrawalSuccess.  # noqa: E501
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success):
        """Sets the success of this WithdrawalSuccess.


        :param success: The success of this WithdrawalSuccess.  # noqa: E501
        :type: bool
        """

        self._success = success

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(WithdrawalSuccess, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, WithdrawalSuccess):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
