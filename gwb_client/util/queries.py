import os
import time
import secrets as s

from dotenv import load_dotenv
import logging
import mysql.connector as cnx


load_dotenv()

DEPOSIT_CONFIRMATIONS = int(os.getenv('DEPOSIT_CONFIRMATIONS', 10))
DB_HOST = os.getenv('BACKEND_DB_HOST')
DB_USER = os.getenv('BACKEND_DB_USER')
DB_PASS = os.getenv('BACKEND_DB_PASS')
DB_NAME = os.getenv('BACKEND_DB_NAME')


class dbConn():
    def __init__(self):
        logging.debug('Database connection opened.')
        self.needs_commit = False
        self.db = cnx.connect(
            host=DB_HOST,
            user=DB_USER,
            passwd=DB_PASS,
            db=DB_NAME,
            charset='utf8mb4',
            collation='utf8mb4_general_ci'
        )

    def q_ro(self, query, args=(), one=False):
        cur = self.db.cursor()
        cur.execute(query, args)
        result = cur.fetchall()
        cur.close()
        return (result[0] if result else []) if one else result

    def q_wo(self, query, args=(), commit_now=False):
        cur = self.db.cursor()
        cur.execute(query, args)
        cur.close()

        if commit_now:
            self.db.commit()
        else:
            self.needs_commit = True

    def __del__(self):
        if self.needs_commit:
            self.db.commit()
        if hasattr(self.db, 'close'):
            self.db.close()
            logging.debug('Database connection closed')


def database_decorator(function):
    def wrapper(*args, **kwargs):
        if not 'db' in kwargs:
            kwargs['db'] = dbConn()
            logging.debug("No database connection was found, injecting one now.")
        return function(*args, **kwargs)
    return wrapper


@database_decorator
def add_deposit(address: str, txid: str, deposit: dict, tx_type='deposit', db=None):
    '''
    :param deposit: Example:
    {
        'amount' : `amount`,
        'confirmations' : `confirmations`,
        'time' : `transaction time`
    }
    '''
    # First check that the deposit doesn't already exist
    if tx_type == 'deposit':
        result = db.q_ro('SELECT count(*) FROM deposit WHERE txid=%s LIMIT 1;', (txid,), one=True)

        if result[0] > 0:
            # Do nothing if it exists
            return

    db.q_wo(
        '''INSERT INTO deposit VALUES (
            DEFAULT, %s, %s,
            %s, %s, %s,
            %s, DEFAULT
        );''',
        (
            txid, address,
            deposit.get('confirmations', 9999), deposit.get('amount', 0.0), deposit.get('time', int(time.time())),
            tx_type
        )
    )


@database_decorator
def wdr_already_made(server_txid, db=None):
    result = db.q_ro('SELECT count(*) FROM withdrawal WHERE external_txid=%s;', (server_txid,), one=True)
    return result[0] > 0


@database_decorator
def add_withdrawal(txid, server_txid, address, amount, db=None):
    db.q_wo(
        '''INSERT INTO withdrawal VALUES (%s, %s, %s, %s, NULL, %s, 1, DEFAULT, DEFAULT);''',
        (server_txid, txid, address, amount, int(time.time()))
    )


@database_decorator
def get_all_txs_to_sync(db=None):
    final = []
    results = db.q_ro(
        'SELECT * FROM deposit WHERE (confirmations<=%s OR fresh=0) AND type="deposit";',
        (DEPOSIT_CONFIRMATIONS,)
    )

    for r in results:
        final.append({
            'txid' : r[1],
            'address' : r[2],
            'confirmations' : r[3],
            'amount' : r[4],
            'time' : r[5]
        })

    return final


@database_decorator
def mark_known_txs(txids: list, db=None):
    for txid in txids:
        db.q_wo('UPDATE deposit SET fresh=1 WHERE txid=%s;', (txid,))


@database_decorator
def update_deposit_confirmations(txid: str, confirmations:int, db=None):
    db.q_wo('UPDATE deposit SET confirmations=%s WHERE txid=%s;', (confirmations, txid))


@database_decorator
def stake_is_new(txid: str, db=None):
    result = db.q_ro('SELECT count(*) FROM deposit WHERE txid=%s AND type="stake";', (txid,), one=True)
    return result[0] == 0
