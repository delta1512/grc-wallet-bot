import requests
import logging
import json
from datetime import datetime
import os

from typing import Optional

from dotenv import load_dotenv

load_dotenv()

RPC_URL = os.getenv('RPC_URL', 'http://localhost:9332/')
RPC_USERNAME = os.getenv('RPC_USERNAME', '')
RPC_PASSWORD = os.getenv('RPC_PASSWORD', '')
WALLET_PASSWORD = os.getenv('WALLET_PASSWORD', '')
TX_LOOKBACK = int(os.getenv('TX_LOOKBACK', 1000))
TX_LOOKBACK_TIME = int(os.getenv('TX_LOOKBACK_TIME', 0))
STK_REWARD = float(os.getenv('STK_REWARD', 10))
DEPOSIT_CONFIRMATIONS = int(os.getenv('DEPOSIT_CONFIRMATIONS', 10))
NET_W_MULT = float(os.getenv('NET_W_MULT', 9544517.41))


def query(cmd: str, params: Optional[list]=None):
    '''
    0 - protocol/client error
    2 - invalid args
    3 - net exception
    4 - invalid type
    '''
    params = params if params else []
    
    if not all([isinstance(cmd, str), isinstance(params, list)]):
        logging.warning('Invalid data sent to wallet query')
        return 2

    command = {'method' : cmd, 'params' : params}

    try:
        r = requests.post(
            RPC_URL,
            json=command,
            headers={'content-type': "application/json", 'cache-control': "no-cache"},
            verify=False,
            auth=requests.auth.HTTPBasicAuth(RPC_USERNAME, RPC_PASSWORD)
        )
    except requests.HTTPError:
        logging.critical('Invalid HTTP encountered during a request')
        return 3
    except requests.ConnectTimeout:
        logging.critical('Wallet backend timed-out when trying to connect to the wallet')
        return 3
    except requests.ReadTimeout:
        logging.critical('The wallet took too long to respond')
        return 3
    except requests.Timeout:
        logging.critical('The wallet took too long to respond')
        return 3
    except requests.ConnectionError:
        logging.critical('Failed to connect to the wallet')
        return 3
    except requests.RequestException:
        logging.critical('An unknown error occurred when making a request to the wallet')
        return 3

    if r.ok:
        response = r.json()
    else:
        logging.warning('Status code %s encountered on command: %s', r.status_code, command)
        return 3

    if response['error'] != None:
        if response['error']['code'] == -17:
            return -17
        if response['error']['code'] == -5:
            return -5
        logging.warning('Error response sent by GRC client: %s', response['error'])
        logging.warning('CMD: %s ARGS: %s', cmd, params)
        return 0
    else:
        return response['result']


def is_in_sync():
    result = query('getinfo')

    if isinstance(result, int):
        return False
    
    return result['in_sync']


def make_withdrawal(addr, amount):
    return query('sendtoaddress', [addr, amount])


def get_block(height=None):
    if height is None:
        height = query('getblockcount')

    data = {}
    block_data = query('getblockbynumber', [height])

    if isinstance(block_data, int):
        return None

    return {
        'height': height,
        'hash': block_data['hash'],
        'time' : block_data['time'],
        'time_formatted': datetime.utcfromtimestamp(block_data['time']).isoformat(' '),
        'difficulty': round(block_data['difficulty'], 4),
        'net_weight': round(NET_W_MULT * block_data['difficulty']),
        'n_txs': len(block_data['tx']),
        'txs' : block_data['tx'],
        'mint': block_data['mint'],
        'superblock': bool(block_data['IsSuperBlock']),
    }


def get_new_address():
    return query('getnewaddress')


def get_latest_stakes():
    data = query('listtransactions', ['', TX_LOOKBACK])
    stakes = {}

    for tx in data:
        if ((tx['category'] == 'generate') and
                (tx['confirmations'] > DEPOSIT_CONFIRMATIONS) and
                (tx['time'] > TX_LOOKBACK_TIME) and
                (tx.get('Type', None) == 'POS')):
            stakes[tx['txid']] = STK_REWARD

    return stakes


def get_confirmations_for_tx(txid):
    tx_data = query('gettransaction', [txid])

    if isinstance(tx_data, int):
        return 0

    return tx_data['confirmations']


def unlock(time_unlocked: int=999999999, staking_only: bool=False):
    result = query(
        'walletpassphrase',
        [WALLET_PASSWORD, time_unlocked, staking_only]
    )

    if result is None:
        return True

    return result


def lock():
    query('walletlock')


def get_version():
    data = query('getinfo')
    return data['version']
