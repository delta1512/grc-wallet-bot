import sys
import getopt
import logging

from gwb_client import client

opts, args = getopt.getopt(sys.argv[1:], 'dwsa', [])

for opt, arg in opts:
    if opt == '-a':
        addresses = client.generate_addresses()
        logging.debug('Addresses submitted to the server successfully: {}'.format(addresses))
    elif opt == '-d':
        deposits = client.collect_deposits()
        logging.debug('Successfully collected the following deposits: {}'.format(deposits))
        client.process_deposits()
    elif opt == '-w':
        withdrawals = client.run_withdrawals()
        logging.debug('Successfully processed the following withdrawals: {}'.format(withdrawals))
    elif opt == '-s':
        client.run_staking()
