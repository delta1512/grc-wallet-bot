import os
import time
import json
import logging
import requests

from dotenv import load_dotenv

import gwb_swagger_client as swagger_client
from gwb_swagger_client.rest import ApiException
from gwb_client.util import wallet as w
from gwb_client.util import queries as q

# Constants setup
load_dotenv()

MIN_DEPOSIT = float(os.getenv('MIN_DEPOSIT', 0.0125))
LAST_BLOCK_FILE = os.getenv('LAST_BLOCK_FILE', 'last_block')

swag_config = swagger_client.Configuration()
swag_config.api_key['backend_token'] = os.getenv('BACKEND_API_TOKEN', '')
swag_config.host = os.getenv('GWB_API_URL', 'localhost')


def post_request_with_payload(req: str, payload: dict, headers: dict=None):
    '''
    Some requests are so large that we need to build a custom request using
    the requests library because the generated client doesn't like it.
    This function constructs a request object, executes it and returns it.

    :param req: The string of the request with a following '/'. This is whatever
        comes after the domain name.
    :param payload: The dictionary containing all the payload data. This will
        be attached to the request as JSON
    :param headers: Optional custom headers. The default will be that which is
        compatible with the GWB server.
    '''
    if headers is None:
        headers = {
            'backend_token' : swag_config.api_key['backend_token'],
            'content-type': 'application/json',
            'cache-control': 'no-cache'
        }

    r = requests.post(
        'https://{}{}'.format(swag_config.host, req),
        headers=headers,
        json=payload
    )

    logging.debug('Request to %s completed with HTTP code %s', req, r.status_code)

    return r


def needs_synced_wallet(func):
    '''
    Decorator that ensures that the wallet is synced before running the function.
    '''
    def needs_synced_wallet_inner(*args, **kwargs):
        if w.is_in_sync():
            return func(*args, **kwargs)
        
        logging.warning("Wallet is not in-sync, not running query.")
    
    return needs_synced_wallet_inner


@needs_synced_wallet
def collect_deposits():
    '''
    Fetches all of the addresses from the API and looks to see if they have
    been recipients of a deposit. If so, they are added to the database.
    '''
    final = {}

    # The from block is where we last scanned from
    with open(LAST_BLOCK_FILE, 'r') as lbf:
        from_block = int(lbf.read())

    # The to block is the latest block
    to_block = w.query('getblockcount')

    if from_block > to_block:
        return final

    # Grab all of the addresses on the server
    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.get_registered_addresses()
        logging.debug('Successfully fetched all addresses')
    except ApiException as e:
        logging.error('Exception when calling get_registered_addresses: %s\n', e)

    resp = resp.to_dict()
    accounts = resp.get('addresses')

    # Prepare the dictionary here to avoid an if-statement later
    for a in accounts:
        final[a] = {}

    for block in range(from_block, to_block + 1):
        logging.debug('Processing block %s', block)

        block_data = w.query('getblockbynumber', [block])
        if block_data.get('confirmations', -1) < 0:
            block_hash = w.query('getblockhash', [block])
            block_data = w.query('getblock', [block_hash])

        for txid in block_data.get('tx', []):
            tx_data = w.query('gettransaction', [txid])

            for output in tx_data.get('vout', []):
                addr = output.get('scriptPubKey', {}).get('addresses', [None])[0]
                amount = output.get('value', 0)
                confs = tx_data.get('confirmations', 0)
                tx_time = tx_data.get('time')
                tx_type = output.get('scriptPubKey', {}).get('type', 'nonstandard')

                # Short circuit intended
                if tx_type == 'pubkeyhash' and addr in accounts:
                    logging.info('Found a deposit in a transaction: %s vout: %s', txid, tx_data.get('n'))

                    if amount < MIN_DEPOSIT:
                        logging.info('Rejecting deposit, amount too small at %s', amount)
                        continue

                    final[addr][txid] = {
                        'amount' : amount,
                        'confirmations' : confs,
                        'time' : tx_time
                    }

    db = q.dbConn()
    for addr in final:
        for txid in final.get(addr):
            q.add_deposit(
                addr,
                txid,
                final.get(addr).get(txid),
                db=db
            )

    # Save where we are up to for next deposit run
    with open(LAST_BLOCK_FILE, 'w') as lbf:
        lbf.write(str(to_block))

    return final


@needs_synced_wallet
def process_deposits():
    '''
    Runs through all unsynced and unconfirmed transactions to update their
    confirmations with the main server. This function checks the wallet for
    the amount of confirmations each transaction has.
    '''
    txs = q.get_all_txs_to_sync()

    # Now check and update each of the txs confirmations
    for i, tx in enumerate(txs):
        txid = tx.get('txid')
        new_confs = w.get_confirmations_for_tx(txid)

        if new_confs > tx.get('confirmations'):
            q.update_deposit_confirmations(txid, new_confs)
            txs[i]['confirmations'] = new_confs

    r = post_request_with_payload('/sync_pending_deposits', {'deposits' : txs})

    if r.ok:
        completed_txids = [tx.get('txid') for tx in txs]
        q.mark_known_txs(completed_txids)
        logging.debug('Successfully pushed unconfirmed deposits')
    else:
        logging.error('Exception when calling sync_pending_deposits: %s\n', r.status_code)

    return txs


@needs_synced_wallet
def run_withdrawals():
    '''
    Fetches all withdrawals to do from the API and performs them. Will send back
    the list of successful and unsuccessful withdrawals.
    '''
    # Grab all of the pending withdrawals on the server
    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.get_withdrawals()
        logging.debug('Successfully fetched all addresses')
    except ApiException as e:
        logging.error('Exception when calling get_withdrawals: %s\n', e)

    resp = resp.to_dict()
    withdrawals = resp.get('withdrawals')
    completed = []

    # Unlock the wallet for withdrawals. This is followed by a lock later on
    w.lock()
    w.unlock(staking_only=False)

    for wdr in withdrawals:
        server_txid = wdr.get('txid')
        address = wdr.get('destination')
        amount = round(wdr.get('amount'), 8)

        if q.wdr_already_made(server_txid):
            logging.warning('Withdrawal %s replayed, marking as failed...', server_txid)
            completed.append({'txid' : server_txid, 'success' : False})
            continue

        result = w.make_withdrawal(address, amount)
        logging.info('Completed a withdrawal with the following result: %s', result)
        time.sleep(1)

        if isinstance(result, int):
            logging.error('Failed to make a withdrawal to %s of amount %s with txid %s', address, amount, server_txid)
            completed.append({'txid' : server_txid, 'success' : False})
        else:
            logging.debug('Withdrawal was successful')
            q.add_withdrawal(result, server_txid, address, amount)
            completed.append({'txid' : server_txid, 'success' : True})

    w.lock()
    w.unlock(staking_only=True)

    # Then tell the main server which withdrawals have been processed
    r = post_request_with_payload(
        '/update_withdrawal_success?withdrawals',
        {'withdrawals' : completed}
    )

    if r.ok:
        logging.info('Running reporting withdrawals successfully')
        return completed
    else:
        logging.warning('Failed to report withdrawals for following transactions: %s', completed)
        return {}


@needs_synced_wallet
def run_staking():
    '''
    Will check if the wallet has staked, if it has, then it will fetch all
    addresses + balances and then send back a list of balances to change.
    '''
    stakes = w.get_latest_stakes()
    new_stakes = {}

    logging.debug('Latest stakes %s', stakes)

    for stake in stakes:
        if q.stake_is_new(stake):
            new_stakes[stake] = stakes[stake]

    if len(new_stakes) > 0:
        # Grab all of the addresses on the server
        api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

        try:
            resp = api.get_registered_addresses_with_balances()
            logging.debug('Successfully fetched all addresses')
        except ApiException as e:
            logging.error('Exception when calling get_registered_addresses_with_balances: %s\n', e)

        resp = resp.to_dict()
        accounts = resp.get('addresses_with_balances')
        accounts_as_dict = {}
        bal_changes = {}

        # Prepare the balance changes, the sum and the dict structure
        s = 0

        for account in accounts:
            bal_changes[account.get('address')] = 0
            s += round(account.get('balance'), 8)
            accounts_as_dict[account.get('address')] = account.get('balance')

        logging.debug('The sum of all eligible balances is %s', s)
    else:
        logging.info('Running stakes completed successfully')
        return {}

    db = q.dbConn()

    for stake in new_stakes:
        val = new_stakes[stake]

        logging.info('Processing stake with txid %s and value %s', stake, val)

        for addr in bal_changes:
            current_bal = accounts_as_dict[addr]
            stake_amt = (current_bal / s) * val
            bal_changes[addr] = round(bal_changes[addr] + stake_amt, 8) #pylint: disable=E4702
            q.add_deposit(
                addr,
                stake,
                {'amount' : stake_amt},
                tx_type='stake',
                db=db
            )

    payload = {'addresses' : [], 'changes' : []}

    # Load the payload
    for addr in bal_changes:
        payload['addresses'].append(addr)
        payload['changes'].append(bal_changes[addr])

    r = post_request_with_payload(
        '/apply_balance_changes?addresses=a&changes=1&is_stake=true',
        payload=payload
    )

    if r.ok:
        logging.info('Running stakes completed successfully')
        return bal_changes
    else:
        logging.warning('Failed to run stakes for following transactions: %s', new_stakes)
        return {}


def generate_addresses(n=3):
    '''
    Generates a number of usable GRC addresses for the wallet bot to issue
    to users.
    '''
    addresses = []

    for i in range(n):
        addresses.append(w.get_new_address())

    api = swagger_client.DefaultApi(swagger_client.ApiClient(swag_config))

    try:
        resp = api.push_new_addresses(addresses)
        logging.debug('Successfully pushed %s new addresses', n)
    except ApiException as e:
        logging.error('Exception when calling push_new_addresses: %s\n', e)

    return addresses
