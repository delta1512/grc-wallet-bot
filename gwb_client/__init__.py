import os
import logging
from logging.handlers import RotatingFileHandler

from dotenv import load_dotenv

load_dotenv()


logging.basicConfig(
    level=int(os.getenv('LOG_LEVEL', logging.DEBUG)),
    format=os.getenv('LOG_FMT'),
    datefmt=os.getenv('LOG_DATE_FMT'),
    handlers=[
        RotatingFileHandler(
            os.path.join(os.getenv('LOG_DIR'), 'gwb_client.log'),
            maxBytes=int(os.getenv('LOG_MAX_BYTES_PER_FILE', logging.DEBUG)),
            backupCount=int(os.getenv('LOG_COUNT', logging.DEBUG))
        )
    ]
)
