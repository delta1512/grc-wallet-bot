CREATE DATABASE IF NOT EXISTS gwbc;

USE gwbc;


CREATE TABLE IF NOT EXISTS deposit (
  did             BIGINT UNSIGNED AUTO_INCREMENT,
  txid            TINYTEXT,
  address         CHAR(34),
  confirmations   INT UNSIGNED,
  amount          DOUBLE UNSIGNED,
  _time           BIGINT UNSIGNED,
  type            CHAR(10) DEFAULT 'deposit' NOT NULL, -- 'deposit' or 'stake'
  fresh           BOOLEAN DEFAULT 0 NOT NULL, -- Whether this deposit needs to be reported to the API server

  PRIMARY KEY (did)
);


CREATE TABLE IF NOT EXISTS withdrawal (
  external_txid   CHAR(64),
  success_txid    TINYTEXT,
  destination     CHAR(34),
  amount          DOUBLE UNSIGNED,
  time_received   BIGINT UNSIGNED, -- Time received by GWBC (not used)
  time_sent       BIGINT UNSIGNED, -- Time executed by GWBC
  executed        BOOLEAN DEFAULT 0, -- Not used
  success         BOOLEAN DEFAULT 0,
  api_known       BOOLEAN DEFAULT 0,

  PRIMARY KEY (external_txid)
);

CREATE INDEX txid ON deposit(txid);
